package api.redirect;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Redirect {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String url;
  public String target;

}

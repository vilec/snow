package api.redirect;

import com.google.inject.Inject;
import com.sun.org.apache.regexp.internal.RE;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class RedirectRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Redirect> findRedirect(String shortLink) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Redirect> criteria = builder.createQuery(Redirect.class);
    Root<Redirect> root = criteria.from(Redirect.class);
    criteria.select(root);
    criteria.where(builder.equal(root.get("url"), shortLink));
    return jpaApi.em().createQuery(criteria).getResultList();
  }
}

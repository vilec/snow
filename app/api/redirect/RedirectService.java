package api.redirect;


import com.google.inject.Inject;

import java.util.List;

public class RedirectService {

  @Inject
  private RedirectRepository repository;

  public String findRedirect(String shortLink) {
    List<Redirect> redirects = repository.findRedirect(shortLink);
    return redirects.isEmpty() ? null : redirects.get(0).target;
  }
}

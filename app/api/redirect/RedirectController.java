package api.redirect;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

public class RedirectController extends Controller {

  @Inject
  private RedirectService redirectService;

  @Transactional
  public Result shortLink(String shortLink) {
    String target = redirectService.findRedirect(shortLink);
    return target != null ? redirect(target) : notFound();
  }

}

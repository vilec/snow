package api.event;

import api.calendar.CalendarService;
import api.category.Category;
import api.company.Company;
import api.event.feature.EventFeature;
import api.event.feature.Feature;
import api.event.transport.EventTransport;
import api.resort.Resort;
import api.user.User;
import api.user.UserService;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static api.event.Filter.Type.*;

public class EventRepository {

  @Inject
  private JPAApi jpaApi;

  @Inject
  private CalendarService calendar;

  @Inject
  private UserService userService;

  public static int EVENTS_PER_PAGE = 10;

  public Long findCountAll(Filters filters) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
    Root<Event> eventRoot = criteria.from(Event.class);

    criteria.select(builder.countDistinct(eventRoot));

    addRestrictionsSorting(filters, builder, criteria, eventRoot);

    return jpaApi.em().createQuery(criteria).getSingleResult();
  }

  public List<Event> findAll(Filters filters, int page) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Event> criteria = builder.createQuery(Event.class);
    Root<Event> eventRoot = criteria.from(Event.class);

    criteria.select(eventRoot);
    criteria.distinct(true);

    addRestrictionsSorting(filters, builder, criteria, eventRoot);

    return jpaApi.em().createQuery(criteria)
      .setFirstResult((page - 1) * EVENTS_PER_PAGE)
      .setMaxResults(EVENTS_PER_PAGE)
      .getResultList();
  }

  public List<Event> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Event> criteria = builder.createQuery(Event.class);
    Root<Event> eventRoot = criteria.from(Event.class);
    criteria.select(eventRoot);

    List<Predicate> restrictions = new ArrayList<>();
    restrictions.add(basicRestrictions(builder, eventRoot));
    criteria.where(restrictions.toArray(new Predicate[]{}));

    return jpaApi.em().createQuery(criteria).getResultList();
  }

  private void addRestrictionsSorting(Filters filters, CriteriaBuilder builder, CriteriaQuery criteria, Root<Event> eventRoot) {
    List<Predicate> restrictions = new ArrayList<>();
    restrictions.add(basicRestrictions(builder, eventRoot));

    if (filters.isPriceFilters()) {
      restrictions.add(applyPriceFilters(filters, builder, eventRoot));
    }

    if (filters.isCountryFilters()) {
      restrictions.add(applyCountryFilters(filters, builder, eventRoot));
    }

    if (filters.isDateFilters()) {
      restrictions.add(applyDatesFilters(filters, builder, eventRoot));
    }

    if (filters.isTransportFilters()) {
      restrictions.add(applyTransportFilters(filters, builder, eventRoot));
    }

    if (filters.isCategoryFilters()) {
      restrictions.add(applyCategoryFilters(filters, builder, eventRoot));
    }

    if (filters.isResortFilters()) {
      restrictions.add(applyResortFilters(filters, builder, eventRoot));
    }

    if (filters.isFeatureFilters()) {
      restrictions.add(applyFeaturesFilters(filters, builder, eventRoot));
    }

    criteria.orderBy(applySorting(filters, builder, eventRoot));
    criteria.where(restrictions.toArray(new Predicate[]{}));
  }

  private Predicate basicRestrictions(CriteriaBuilder builder, Root<Event> eventRoot) {
    Join<Event, Company> companyJoin = eventRoot.join("company");
    return builder.and(
        builder.isTrue(companyJoin.get("active")),
        builder.isTrue(eventRoot.get("active")),
        builder.greaterThanOrEqualTo(eventRoot.get("start"), calendar.getCurrentDate())
    );
  }

  private Predicate applyFeaturesFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Predicate> restrictions = new ArrayList<>();
    List<String> featureTypes = filters.getFilters(FEATURE).stream().map(f -> (String) f.getValue()).collect(Collectors.toList());
    featureTypes.forEach(feature -> {
      Join<Event, EventFeature> eventFeatures = eventRoot.join("features");
      Join<EventFeature, Feature> features = eventFeatures.join("feature");
      restrictions.add(builder.equal(features.get("code"), feature));
    });
    return builder.and(restrictions.toArray(new Predicate[]{}));
  }

  private Predicate applyResortFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Integer> resortIds = filters.getFilters(RESORT).stream().map(id -> (int) id.getValue()).collect(Collectors.toList());
    return eventRoot.get("resort").get("id").in(resortIds);
  }

  private List<Order> applySorting(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    switch (filters.getSortFilter()) {
      case SORT_DATE_ASC:
        return Arrays.asList(builder.asc(eventRoot.get("start")));
      case SORT_DATE_DESC:
        return Arrays.asList(builder.desc(eventRoot.get("start")));
      case SORT_PRICE_ASC:
        return Arrays.asList(builder.asc(eventRoot.get("discountPrice")), builder.asc(eventRoot.get("start")));
      case SORT_PRICE_DESC:
        return Arrays.asList(builder.desc(eventRoot.get("discountPrice")), builder.asc(eventRoot.get("start")));
    }
    return null;
  }

  private Predicate applyTransportFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Predicate> restrictions = new ArrayList<>();
    Join<Event, EventTransport> eventTransports = eventRoot.join("transports");
    Join<EventTransport, Transport> transports = eventTransports.join("transport");
    List<String> transportTypes = filters.getFilters(TRANSPORT).stream().map(f -> (String) f.getValue()).collect(Collectors.toList());
    restrictions.add(transports.get("code").in(transportTypes));
    return builder.or(restrictions.toArray(new Predicate[]{}));
  }

  private Predicate applyDatesFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Predicate> restrictions = new ArrayList<>();
    for (Filter filter : filters.getFilters(DATE)) {
      restrictions.add(builder.or(
        builder.between(eventRoot.get("start"), calendar.getFromDate(filter), calendar.getToDate(filter)),
        builder.between(eventRoot.get("end"), calendar.getFromDate(filter), calendar.getToDate(filter))
      ));
    }
    return builder.or(restrictions.toArray(new Predicate[]{}));
  }

  private Predicate applyCountryFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Predicate> countryRestrictions = new ArrayList<>();
    Join<Event, Resort> resort = eventRoot.join("resort");
    for (Filter filter : filters.getFilters(COUNTRY)) {
      countryRestrictions.add(builder.equal(resort.get("country"), filter.getValue()));
    }
    return builder.or(countryRestrictions.toArray(new Predicate[]{}));
  }

  private Predicate applyCategoryFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Predicate> categoryFilters = new ArrayList<>();
    Join<Event, Category> category = eventRoot.join("category");
    for (Filter filter : filters.getFilters(CATEGORY)) {
      categoryFilters.add(builder.equal(category.get("code"), filter.getValue()));
    }
    return builder.or(categoryFilters.toArray(new Predicate[]{}));
  }

  private Predicate applyPriceFilters(Filters filters, CriteriaBuilder builder, Root<Event> eventRoot) {
    List<Predicate> priceRestrictions = new ArrayList<>();
    for (Filter filter : filters.getFilters(PRICE)) {
      priceRestrictions.add(builder.between(eventRoot.get("price"),
        Integer.valueOf(filter.getFrom()), Integer.valueOf(filter.getTo())));
    }
    return builder.or(priceRestrictions.toArray(new Predicate[]{}));
  }

  public Event find(long eventId) {
    return jpaApi.em().find(Event.class, eventId);
  }

  public List<Event> findByLink(String link) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Event> criteria = builder.createQuery(Event.class);
    Root<Event> eventRoot = criteria.from(Event.class);
    criteria.select(eventRoot);
    criteria.where(builder.equal(eventRoot.get("link"), link));
    return jpaApi.em().createQuery(criteria)
      .getResultList();
  }

  public List<Event> findAllPerUser() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Event> criteria = builder.createQuery(Event.class);
    Root<Event> eventRoot = criteria.from(Event.class);
    criteria.select(eventRoot);

    if (!userService.getLoggedUser().isAdmin()) {
      Join<Event, Company> company = eventRoot.join("company");
      Join<Company, User> user = company.join("user");
      List<Predicate> restrictions = new ArrayList<>();
      restrictions.add(builder.equal(user.get("id"), userService.getLoggedUser().id));
      criteria.where(restrictions.toArray(new Predicate[]{}));
    }

    return jpaApi.em().createQuery(criteria).getResultList();
  }

  public Event update(Event event) {
    //event.prices.forEach(p -> jpaApi.em().merge(p));
    return jpaApi.em().merge(event);
  }

}

package api.event.transport;

import api.event.Transport;
import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class TransportController {

  @Inject
  private TransportService service;

  @Transactional
  public Result listAll() throws InterruptedException {
    List<Transport> events = service.findAll();
    return ok(toJson(events));
  }
}

package api.event.transport;

import api.event.Event;
import api.event.Transport;
import api.event.price.EventPrice;
import api.event.price.Price;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "event_transport")
public class EventTransport {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  @JsonIgnore
  public Event event;

  @ManyToOne
  @JoinColumn(name="transport_id")
  public Transport transport;

  @Enumerated(EnumType.STRING)
  public TransportKind kind;
  public int value;
  public String currency;

  enum TransportKind {
    INCLUDED, DISCOUNT, EXTRA, ON_YOUR_OWN, ON_DEMAND;
  }

  public boolean isExtra(){
    return this.kind == TransportKind.EXTRA;
  }
  public boolean isDiscount(){
    return this.kind == TransportKind.DISCOUNT;
  }
  public boolean isOnYourOwn(){
    return this.kind == TransportKind.ON_YOUR_OWN;
  }
  public boolean isOnDemand(){
    return this.kind == TransportKind.ON_DEMAND;
  }
  public boolean isIncluded(){
    return this.kind == TransportKind.INCLUDED;
  }
}

package api.event.transport;

import api.event.Transport;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class TransportRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Transport> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Transport> criteria = builder.createQuery(Transport.class);
    Root<Transport> eventRoot = criteria.from(Transport.class);
    criteria.select(eventRoot);
    //criteria.orderBy(builder.asc(eventRoot.get("order")));
    return jpaApi.em().createQuery(criteria).getResultList();
  }
}

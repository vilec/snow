package api.event.transport;


import api.event.Transport;
import com.google.inject.Inject;

import java.util.List;

public class TransportService {

  @Inject
  private TransportRepository repository;

  public List<Transport> findAll() {
    return repository.findAll();
  }
}

package api.event;


import api.company.Company;
import api.company.CompanyRepository;
import api.resort.Resort;
import api.resort.ResortRepository;
import api.user.UserService;
import com.google.inject.Inject;
import org.hibernate.Hibernate;
import play.Logger;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static api.event.Filter.*;

public class EventService {

  @Inject
  private EventRepository repository;

  @Inject
  private CompanyRepository companyRepository;

  @Inject
  private ResortRepository resortRepository;

  @Inject
  private UserService user;

  @Inject
  private JPAApi jpaApi;

  public SearchResult findAll(String query, String page) {
    Filters filters = convert(query);
    int currentPage = convertPage(page);
    long total = repository.findCountAll(filters);
    List<Event> events = repository.findAll(filters, currentPage);
    return new SearchResult(events, total);
  }

  private int convertPage(String page) {
    int currentPage = 1;
    try {
      currentPage = Integer.parseInt(page);
    } catch (NumberFormatException e) {
      Logger.warn("'page' parameter has illegal format: " + page, e);
    }
    return currentPage;
  }

  private Filters convert(String query) {
    if (query == null || query.isEmpty()) {
      return new Filters(new ArrayList<>());
    }
    List<String> queryFilters = Arrays.asList(query.split(","));
    return new Filters(queryFilters.stream().map(q -> Filter.valueOf(q.toUpperCase())).collect(Collectors.toList()));
  }

  public Event find(long eventId) {
    Event event = repository.find(eventId);
    return prepareEvent(event);
  }

  public Event find(String link) {
    Event event = repository.findByLink(link).get(0);
    return prepareEvent(event);
  }

  private Event prepareEvent(Event event) {
    event.prices.sort((a, b) -> (a.price.order < b.price.order ? -1 : 1));
    return event;
  }

  public List<Event> findAllPerUser() {
    return repository.findAllPerUser();
  }

  @Transactional
  public List<Event> findAll() {
    return repository.findAll();
  }

  public Event update(Event event) {
    if (!user.getLoggedUser().isAdmin()) {
      event.company = new Company();
      event.company.id = user.getLoggedUser().company.id;
    }
    event.link = generateLink(event);
    event.prices.forEach(p -> p.event = event);
    event.transports.forEach(t -> t.event = event);
    event.accommodations.forEach(a -> a.event = event);
    event.features.forEach(f -> f.event = event);
    event.images.forEach(i -> i.event = event);
    return repository.update(event);
  }

  private String generateLink(Event event) {
    Company company = companyRepository.find(event.company.id);
    Resort resort = resortRepository.find(event.resort.id);
    Hibernate.initialize(company);
    Hibernate.initialize(resort);
    int linkAppender = 0;

    String newLink = new Slugify().slugify(company.getName() + " " + event.name + " " + resort.getFullName());
    List<Event> sameLink = repository.findByLink(newLink);
    while (sameLink.size() > 1 || (sameLink.size() == 1 && !sameLink.get(0).id.equals(event.id))) {
      newLink = new Slugify().slugify(company.getName() + " " + event.name + " " + resort.getFullName());
      newLink += "-" + ++linkAppender;
      sameLink = repository.findByLink(newLink);
    }
    return newLink;
  }

  public Event copyEvent(Event event) {
    event.id = null;
    event.prices.forEach(p -> p.id = 0);
    event.transports.forEach(t -> t.id = 0);
    event.accommodations.forEach(a -> a.id = 0);
    event.features.forEach(f -> f.id = 0);
    event.images.forEach(i -> i.id = 0);
    jpaApi.em().detach(event);
    return update(event);
  }
}

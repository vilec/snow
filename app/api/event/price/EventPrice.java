package api.event.price;

import api.event.Event;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "event_price")
public class EventPrice {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  @JsonIgnore
  public Event event;

  @ManyToOne
  @JoinColumn(name="price_id")
  public Price price;

  @Enumerated(EnumType.STRING)
  public PriceKind kind;
  public Integer value;
  public String currency;
  public String description;

  enum PriceKind {
    INCLUDED, EXCLUDED, OPTIONAL, RETURNED, N_A;
  }

  public boolean isIncluded() {
    return this.kind == PriceKind.INCLUDED;
  }

  public boolean isExcluded() {
    return this.kind == PriceKind.EXCLUDED;
  }

  public boolean isOptional() {
    return this.kind == PriceKind.OPTIONAL;
  }

  public boolean isReturned() {
    return this.kind == PriceKind.RETURNED;
  }

  public boolean isNA() {
    return this.kind == PriceKind.N_A;
  }

}

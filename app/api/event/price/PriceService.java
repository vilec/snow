package api.event.price;


import com.google.inject.Inject;

import java.util.List;

public class PriceService {

  @Inject
  private PriceRepository repository;

  public List<Price> findAll() {
    return repository.findAll();
  }
}

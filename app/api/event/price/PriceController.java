package api.event.price;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class PriceController {

  @Inject
  private PriceService priceService;

  @Transactional
  public Result listAll() throws InterruptedException {
    List<Price> events = priceService.findAll();
    return ok(toJson(events));
  }
}

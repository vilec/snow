package api.event.price;

import javax.persistence.*;

@Entity
public class Price {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String name;
  public int order;
  public String code;
}

package api.event.feature;

import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class FeatureRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Feature> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Feature> criteria = builder.createQuery(Feature.class);
    Root<Feature> eventRoot = criteria.from(Feature.class);
    criteria.select(eventRoot);
    //criteria.orderBy(builder.asc(eventRoot.get("order")));
    return jpaApi.em().createQuery(criteria).getResultList();
  }
}

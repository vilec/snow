package api.event.feature;

import api.event.Event;
import api.event.Transport;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "event_feature")
public class EventFeature {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  @JsonIgnore
  public Event event;

  @ManyToOne
  @JoinColumn(name="feature_id")
  public Feature feature;
}

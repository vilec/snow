package api.event.feature;

import javax.persistence.*;

@Entity
public class Feature {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String code;
  public String name;
  public String icon;

}

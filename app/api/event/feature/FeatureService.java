package api.event.feature;


import com.google.inject.Inject;

import java.util.List;

public class FeatureService {

  @Inject
  private FeatureRepository repository;

  public List<Feature> findAll() {
    return repository.findAll();
  }
}

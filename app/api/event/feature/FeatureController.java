package api.event.feature;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class FeatureController {

  @Inject
  private FeatureService service;

  @Transactional
  public Result listAll() throws InterruptedException {
    List<Feature> events = service.findAll();
    return ok(toJson(events));
  }
}

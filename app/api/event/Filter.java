package api.event;


import static api.event.Filter.Type.*;
import static api.event.Filter.Year.*;

public enum Filter {
  ALL_TERMS(DATE, true, null, null, null),
  OCTOBER(DATE, false, "10-01", "10-31", THIS_YEAR),
  NOVEMBER(DATE, false, "11-01", "11-30", THIS_YEAR),
  DECEMBER(DATE, false, "12-01", "12-31", THIS_YEAR),
  NEW_YEAR(DATE, false, "12-24", "01-04", TO_NEXT_YEAR),
  JANUARY(DATE, false, "01-01", "01-31", FROM_TO_NEXT_YEAR),
  FEBRUARY(DATE, false, "02-01", "02-28", FROM_TO_NEXT_YEAR),
  MARCH(DATE, false, "03-01", "03-31", FROM_TO_NEXT_YEAR),
  APRIL(DATE, false, "04-01", "04-30", FROM_TO_NEXT_YEAR),
  MAY(DATE, false, "05-01", "05-31", FROM_TO_NEXT_YEAR),

  ALL_COUNTRIES(COUNTRY, true, null, null, null),
  GEORGIA(COUNTRY, false, null, null, Country.GEORGIA),
  ITALY(COUNTRY, false, null, null, Country.ITALY),
  POLAND(COUNTRY, false, null, null, Country.POLAND),
  AUSTRIA(COUNTRY, false, null, null, Country.AUSTRIA),
  FRANCE(COUNTRY, false, null, null, Country.FRANCE),
  CZECH(COUNTRY, false, null, null, Country.CZECH),
  SWITZERLAND(COUNTRY, false, null, null, Country.SWITZERLAND),

  ALL_TRANSPORTS(TRANSPORT, true, null, null, null),
  CAR(TRANSPORT, false, null, null, "CAR"),
  BUS(TRANSPORT, false, null, null, "BUS"),
  PLANE(TRANSPORT, false, null, null, "PLANE"),

  ALL_PRICES(PRICE, true, null, null, null),
  LESS_900(PRICE, false, "0", "900", null),
  BETWEEN_900_1200(PRICE, false, "900", "1200", null),
  BETWEEN_1200_1500(PRICE, false, "1200", "1500", null),
  BETWEEN_1500_2000(PRICE, false, "1500", "2000", null),
  BETWEEN_2000_2500(PRICE, false, "2000", "2500", null),
  MORE_2500(PRICE, false, "2500", "10000", null),

  ALL_FEATURES(FEATURE, true, null, null, null),
  FEATURE_BOARD_GAMES(FEATURE, false, null, null, "BOARD_GAMES"),
  FEATURE_WORKSHOPS(FEATURE, false, null, null, "WORKSHOPS"),
  FEATURE_ICERINK(FEATURE, false, null, null, "ICERINK"),
  FEATURE_SPA(FEATURE, false, null, null, "SPA"),
  FEATURE_PARTY(FEATURE, false, null, null, "PARTY"),
  FEATURE_TOURNAMENTS(FEATURE, false, null, null, "TOURNAMENTS"),
  FEATURE_MUSIC(FEATURE, false, null, null, "MUSIC"),
  FEATURE_EQUIPMENT_TESTS(FEATURE, false, null, null, "EQUIPMENT_TESTS"),
  FEATURE_PARAGLIDER(FEATURE, false, null, null, "PARAGLIDER"),
  FEATURE_APRES_SKI(FEATURE, false, null, null, "APRES_SKI"),
  FEATURE_TRAINING(FEATURE, false, null, null, "TRAINING"),
  FEATURE_GOPRO(FEATURE, false, null, null, "GOPRO"),
  FEATURE_POKER(FEATURE, false, null, null, "POKER"),

  ALL_CATEGORY(CATEGORY, true, null, null, null),
  ADULT(CATEGORY, false, null, null, "ADULT"),
  FREERIDE(CATEGORY, false, null, null, "FREERIDE"),
  FAMILY(CATEGORY, false, null, null, "FAMILY"),
  CHILDREN(CATEGORY, false, null, null, "CHILDREN"),
  TRAINING(CATEGORY, false, null, null, "TRAINING"),

  RESORT_1(RESORT, false, null, null, 1),
  RESORT_2(RESORT, false, null, null, 2),
  RESORT_3(RESORT, false, null, null, 3),
  RESORT_4(RESORT, false, null, null, 4),
  RESORT_5(RESORT, false, null, null, 5),
  RESORT_6(RESORT, false, null, null, 6),
  RESORT_7(RESORT, false, null, null, 7),
  RESORT_8(RESORT, false, null, null, 8),
  RESORT_9(RESORT, false, null, null, 9),
  RESORT_10(RESORT, false, null, null, 10),
  RESORT_11(RESORT, false, null, null, 11),
  RESORT_12(RESORT, false, null, null, 12),
  RESORT_13(RESORT, false, null, null, 13),
  RESORT_14(RESORT, false, null, null, 14),
  RESORT_15(RESORT, false, null, null, 15),
  RESORT_16(RESORT, false, null, null, 16),
  RESORT_17(RESORT, false, null, null, 17),
  RESORT_18(RESORT, false, null, null, 18),
  RESORT_19(RESORT, false, null, null, 19),
  RESORT_20(RESORT, false, null, null, 20),
  RESORT_21(RESORT, false, null, null, 21),
  RESORT_22(RESORT, false, null, null, 22),
  RESORT_23(RESORT, false, null, null, 23),
  RESORT_24(RESORT, false, null, null, 24),
  RESORT_25(RESORT, false, null, null, 25),
  RESORT_26(RESORT, false, null, null, 26),
  RESORT_27(RESORT, false, null, null, 27),
  RESORT_28(RESORT, false, null, null, 28),
  RESORT_29(RESORT, false, null, null, 29),
  RESORT_30(RESORT, false, null, null, 30),
  RESORT_31(RESORT, false, null, null, 31),
  RESORT_32(RESORT, false, null, null, 32),
  RESORT_33(RESORT, false, null, null, 33),
  RESORT_34(RESORT, false, null, null, 34),
  RESORT_35(RESORT, false, null, null, 35),
  RESORT_36(RESORT, false, null, null, 36),
  RESORT_37(RESORT, false, null, null, 37),
  RESORT_38(RESORT, false, null, null, 38),
  RESORT_39(RESORT, false, null, null, 39),
  RESORT_40(RESORT, false, null, null, 40),
  RESORT_41(RESORT, false, null, null, 41),
  RESORT_42(RESORT, false, null, null, 42),
  RESORT_43(RESORT, false, null, null, 43),
  RESORT_44(RESORT, false, null, null, 44),
  RESORT_45(RESORT, false, null, null, 45),
  RESORT_46(RESORT, false, null, null, 46),
  RESORT_47(RESORT, false, null, null, 47),


  SORT_DATE_ASC(SORT, false, null, null, true),
  SORT_DATE_DESC(SORT, false, null, null, false),
  SORT_PRICE_ASC(SORT, false, null, null, true),
  SORT_PRICE_DESC(SORT, false, null, null, false),;

  private final Type type;
  private final boolean all;
  private final String from;
  private final String to;
  private final Object value;

  Filter(Type type, boolean all, String from, String to, Object value) {
    this.type = type;
    this.all = all;
    this.from = from;
    this.to = to;
    this.value = value;
  }

  public Type getType() {
    return type;
  }

  public boolean isAll() {
    return all;
  }

  public String getFrom() {
    return from;
  }

  public String getTo() {
    return to;
  }

  public Object getValue() {
    return value;
  }

  enum Type {
    DATE, COUNTRY, TRANSPORT, PRICE, FEATURE, CATEGORY, RESORT, SORT
  }

  public enum Year {
    FROM_NEXT_YEAR(1, 0), TO_NEXT_YEAR(0, 1), FROM_TO_NEXT_YEAR(1, 1), THIS_YEAR(0, 0);

    private int from;
    private int to;

    Year(int from, int to) {
      this.from = from;
      this.to = to;
    }

    public int getFrom() {
      return from;
    }

    public int getTo() {
      return to;
    }
  }

}

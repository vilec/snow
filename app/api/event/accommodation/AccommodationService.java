package api.event.accommodation;


import com.google.inject.Inject;

import java.util.List;

public class AccommodationService {

  @Inject
  private AccommodationRepository repository;

  public List<Accommodation> findAll() {
    return repository.findAll();
  }
}

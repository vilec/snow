package api.event.accommodation;

import javax.persistence.*;

@Entity
public class Accommodation {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String name;
  public String code;
  public int order;
}

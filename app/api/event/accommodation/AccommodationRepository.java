package api.event.accommodation;

import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class AccommodationRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Accommodation> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Accommodation> criteria = builder.createQuery(Accommodation.class);
    Root<Accommodation> eventRoot = criteria.from(Accommodation.class);
    criteria.select(eventRoot);
    criteria.orderBy(builder.asc(eventRoot.get("order")));
    return jpaApi.em().createQuery(criteria).getResultList();
  }
}

package api.event.accommodation;

import api.event.Event;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "event_accommodation")
public class EventAccommodation {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  @JsonIgnore
  public Event event;

  @ManyToOne
  @JoinColumn(name="accommodation_id")
  public Accommodation accommodation;

  public boolean offered;

  public boolean isAccessible() {
    return offered;
  }
}

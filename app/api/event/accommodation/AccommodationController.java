package api.event.accommodation;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class AccommodationController {

  @Inject
  private AccommodationService service;

  @Transactional
  public Result listAll() throws InterruptedException {
    List<Accommodation> events = service.findAll();
    return ok(toJson(events));
  }
}

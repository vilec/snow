package api.event;


import java.util.List;

public class SearchResult {

  public List<Event> events;
  public long total;

  public SearchResult(List<Event> events, long total) {
    this.events = events;
    this.total = total;
  }
}

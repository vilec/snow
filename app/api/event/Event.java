package api.event;

import api.calendar.CustomerDateDeserialize;
import api.category.Category;
import api.company.Company;
import api.event.accommodation.EventAccommodation;
import api.event.feature.EventFeature;
import api.event.price.EventPrice;
import api.event.transport.EventTransport;
import api.resort.Resort;
import api.user.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
public class Event {

  private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long id;

  public boolean active;

  public String name;

  @JsonDeserialize(using = CustomerDateDeserialize.class)
  public Date start;

  @JsonDeserialize(using = CustomerDateDeserialize.class)
  public Date end;

  @Formula(value = "(select price - c.discount from event e join company c on c.id = e.company_id where e.id = id)")
  public int discountPrice;
  public int price;

  @Column(name = "price_notes")
  public String priceNotes;

  @Column(name = "price_from")
  public boolean priceFrom;

  public Integer people;
  public int stars;
  public String rooms;
  public String url;
  public String link;

  public String notes;

  @ManyToOne
  @JoinColumn(name = "resort_id")
  public Resort resort;

  @ManyToOne
  @JoinColumn(name = "category_id")
  public Category category;

  @ManyToOne
  @JoinColumn(name = "company_id", referencedColumnName = "id")
  public Company company;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval=true)
  public List<EventFeature> features;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval=true)
  public List<EventTransport> transports;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval=true)
  public List<EventAccommodation> accommodations;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
  public List<EventPrice> prices;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval=true)
  public List<EventImage> images;

  public String getStart() {
    return start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(formatter);
  }

  public String getEnd() {
    return end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(formatter);
  }

  public String getStarsDescription() {
    switch (stars) {
      case 1:
        return "Podstawowy";
      case 2:
        return "Dobry";
      case 3:
        return "Bardzo dobry";
      case 4:
        return "Doskonały";
      case 5:
        return "Rewelacyjny";
    }
    return "";
  }

  public List<String> peopleInRoom(){
    return new ArrayList<>(Arrays.asList(this.rooms.split(",")));
  }

  public boolean isOwnedBy(User loggedUser) {
    return loggedUser.isAdmin() || company.user.id == loggedUser.id;
  }
}

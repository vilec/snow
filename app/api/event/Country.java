package api.event;

public enum Country {
   POLAND("Polska", "211-poland.svg"),
   ITALY("Włochy", "013-italy.svg"),
   AUSTRIA("Austria", "003-austria.svg"),
   CZECH("Czechy", "149-czech-republic.svg"),
   FRANCE("Francja", "195-france.svg"),
   SWITZERLAND("Szwajcaria", "205-switzerland.svg"),
   GEORGIA("Gruzja", "256-georgia.svg"),
   SLOVAKIA("Słowacja", "091-slovakia.svg"),
   SLOVENIA("Słowenia", "010-slovenia.svg");

  private final String name;
  private final String flag;

  Country(String name, String flag){
     this.name = name;
     this.flag = flag;
   }

  public String getName() {
    return name;
  }

  public String getFlag() {
    return flag;
  }
}

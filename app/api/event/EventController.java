package api.event;

import api.user.UserService;
import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;

public class EventController extends Controller {

  @Inject
  private EventService eventService;

  @Inject
  private UserService user;

  @Transactional
  public Result search(String query, String page) throws InterruptedException {
    SearchResult events = eventService.findAll(query, page);
    return ok(toJson(events));
  }

  @Transactional
  public Result showEvent(String link) {
    Event event = eventService.find(link);
    if (!event.company.active) {
      return notFound();
    }
    return ok(views.html.event.render(event));
  }

  @Transactional
  public Result update() {
    Event event = Json.fromJson(request().body().asJson(), Event.class);
    if (event.id != null) {
      Event oldEvent = eventService.find(event.id);
      if (!oldEvent.isOwnedBy(user.getLoggedUser())) {
        return unauthorized();
      }
    }
    eventService.update(event);
    return ok();
  }

  @Transactional
  public Result listAllPerUser() {
    List<Event> events = eventService.findAllPerUser();
    return ok(toJson(events));
  }

  @Transactional
  public Result findEvent(long eventId) {
    Event event = eventService.find(eventId);
    if (!event.isOwnedBy(user.getLoggedUser())) {
      return unauthorized();
    }
    return ok(toJson(event));
  }

  @Transactional
  public Result copyEvent(long eventId) {
    Event event = eventService.find(eventId);
    if (!event.isOwnedBy(user.getLoggedUser())) {
      return unauthorized();
    }
    Event copiedEvent = eventService.copyEvent(event);
    return ok(toJson(copiedEvent));
  }

}

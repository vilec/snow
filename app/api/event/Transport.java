package api.event;

import javax.persistence.*;

@Entity
public class Transport {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  public long id;

  public String name;
  public String code;
  public String icon;

}

package api.event;

import java.util.List;
import java.util.stream.Collectors;

public class Filters {

  private final List<Filter> filters;

  public Filters(List<Filter> filters) {
    this.filters = filters;
  }

  public List<Filter> getFilters() {
    return filters;
  }

  public boolean isPriceFilters() {
    return !getFilters(Filter.Type.PRICE).isEmpty();
  }

  public List<Filter> getFilters(Filter.Type type) {
    return filters.stream().filter(f -> f.getType().equals(type)).collect(Collectors.toList());
  }

  public boolean isCountryFilters() {
    return !getFilters(Filter.Type.COUNTRY).isEmpty();
  }

  public boolean isDateFilters() {
    return !getFilters(Filter.Type.DATE).isEmpty();
  }

  public boolean isTransportFilters() {
    return !getFilters(Filter.Type.TRANSPORT).isEmpty();
  }

  public boolean isCategoryFilters() {
    return !getFilters(Filter.Type.CATEGORY).isEmpty();
  }

  public Filter getSortFilter() {
    List<Filter> sorting = getFilters(Filter.Type.SORT);
    return sorting.isEmpty() ? Filter.SORT_DATE_ASC : sorting.get(0);
  }

  public boolean isFeatureFilters() {
    return !getFilters(Filter.Type.FEATURE).isEmpty();
  }

  public boolean isResortFilters() {
    return !getFilters(Filter.Type.RESORT).isEmpty();
  }
}

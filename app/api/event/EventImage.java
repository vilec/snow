package api.event;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "event_image")
public class EventImage {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  @JsonIgnore
  public Event event;

  public String path;
  public String name;
}

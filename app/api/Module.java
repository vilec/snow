package api;

import api.monitoring.CheckActor;
import api.monitoring.CheckAllActor;
import api.monitoring.CheckAllTask;
import com.google.inject.AbstractModule;
import play.libs.akka.AkkaGuiceSupport;

public class Module extends AbstractModule implements AkkaGuiceSupport {
    @Override
    protected void configure() {
        bindActor(CheckAllActor.class, "check-all-actor");
        bindActor(CheckActor.class, "check-actor");
        bind(CheckAllTask.class).asEagerSingleton();
    }
}

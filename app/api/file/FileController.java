package api.file;


import api.user.UserService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;

import static play.libs.Json.toJson;
import static play.mvc.Controller.flash;
import static play.mvc.Controller.request;
import static play.mvc.Results.*;

public class FileController {

  @Inject
  public UserService service;

  public Result upload() {
    Http.MultipartFormData<File> body = request().body().asMultipartFormData();
    Http.MultipartFormData.FilePart<File> picture = body.getFile("file");
    if (picture != null) {
      String originalFileName = picture.getFilename();
      String realFileName = generateRandomFilename(originalFileName);
      File file = picture.getFile();
      String myUploadPath = Play.application().configuration().getString("imagesUploadPath");
      File dest = new File(myUploadPath, realFileName);
      if (file.renameTo(dest)) {
        Logger.info("File moved to: " + dest.getAbsolutePath());
        ObjectNode result = Json.newObject();
        result.put("realFileName", realFileName);
        result.put("originalFileName", originalFileName);
        return ok(toJson(result));
      } else {
        flash("error", "Moving file failed");
        return badRequest();
      }
    } else {
      flash("error", "Missing file");
      return badRequest();
    }
  }

  public String generateRandomFilename(String originalFileName) {
    String extension = FilenameUtils.getExtension(originalFileName);
    return service.getLoggedUser().id + System.currentTimeMillis() + "." + extension;
  }

  public Result get(String filePath) {
    return getFile(filePath, "imagesUploadPath");
  }

  public Result getResort(String filePath) {
    return getFile(filePath, "resortsPath");
  }

  private Result getFile(String filePath, String propertyKey) {
    String myUploadPath = Play.application().configuration().getString(propertyKey);
    File file = new File(myUploadPath + "/" + filePath);
    if (file.exists()) {
      return ok(file, true);
    } else {
      return notFound();
    }
  }

}

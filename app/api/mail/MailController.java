package api.mail;

import api.security.SslEnforced;
import com.google.inject.Inject;
import play.data.Form;
import play.data.FormFactory;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;

@SslEnforced
public class MailController extends Controller {

  @Inject
  MailerClient mailerClient;

  @Inject
  FormFactory formFactory;

  public Result mail() {
    Form<EmailRequest> emailForm = formFactory.form(EmailRequest.class);
    EmailRequest request = emailForm.bindFromRequest().get();

    Email email = new Email()
      .setSubject("SnowTribe.pl - Wiadomość z formularza kontaktowego")
      .setFrom("Formularz kontaktowy <kontakt@snowtribe.pl>")
      .addTo("<kontakt@snowtribe.pl>")
      .setReplyTo(request.getEmail())
      .setBodyText(request.getMessage());
    mailerClient.send(email);
    return ok("Wiadomość została pomyślnie wysłana!");
  }


}

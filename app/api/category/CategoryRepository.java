package api.category;

import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CategoryRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Category> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Category> criteria = builder.createQuery(Category.class);
    Root<Category> root = criteria.from(Category.class);
    criteria.select(root);
    return jpaApi.em().createQuery(criteria).getResultList();
  }
}

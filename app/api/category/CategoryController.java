package api.category;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class CategoryController {

  @Inject
  private CategoryService categoryService;

  @Transactional
  public Result listAll() {
    return ok(toJson(categoryService.findAll()));
  }

}

package api.category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Category {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  public long id;

  public String name;
  public String code;
  public String icon;

}

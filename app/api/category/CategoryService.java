package api.category;


import com.google.inject.Inject;

import java.util.List;

public class CategoryService {

  @Inject
  private CategoryRepository repository;

  public List<Category> findAll() {
    return repository.findAll();
  }
}

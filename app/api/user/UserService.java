package api.user;

import api.company.Company;
import com.google.inject.*;
import java.util.*;
import org.pac4j.core.profile.*;
import org.pac4j.play.*;
import org.pac4j.play.store.*;
import play.mvc.*;

public class UserService {

  @Inject
  private PlaySessionStore playSessionStore;

  public User getLoggedUser() {
    PlayWebContext webContext = new PlayWebContext(Http.Context.current(), playSessionStore);
    ProfileManager<CommonProfile> profileManager = new ProfileManager(webContext);
    Optional<CommonProfile> profile = profileManager.get(false);
    User user = new User();
    user.id = ((Long) profile.get().getAttribute("id"));
    user.admin = ((boolean) profile.get().getAttribute("admin"));
    user.company = new Company();
    user.company.id = ((long) profile.get().getAttribute("company"));
    return user;
  }
}

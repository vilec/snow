package api.user;


import api.user.*;
import com.google.inject.*;
import java.util.*;
import play.db.jpa.*;

public class UserRepository {

  @Inject
  private JPAApi jpaApi;

  public List<User> findUsers(String username) {
    return jpaApi.em().createQuery("select u from User u where username = :username", User.class)
      .setParameter("username", username)
      .getResultList();
  }
}

package api.user;


import api.company.Company;
import api.security.*;
import javax.persistence.*;
import org.mindrot.jbcrypt.*;
import org.pac4j.core.profile.*;
import org.pac4j.jwt.config.signature.*;
import org.pac4j.jwt.profile.*;

@Entity
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;
  public String username;
  public String password;
  public String email;
  public boolean admin;

  @OneToOne
  @JoinColumn(name = "company_id", referencedColumnName = "id")
  public Company company;

  public boolean validatePassword(String candidate) {
    return BCrypt.checkpw(candidate, password);
  }

  public String generateToken() {
    CommonProfile profile = new CommonProfile();
    profile.addAttribute("username", username);
    profile.addAttribute("admin", admin);
    profile.addAttribute("email", email);
    profile.addAttribute("id", id);
    profile.addAttribute("name", company.name);
    profile.addAttribute("logo", company.logo);
    profile.addAttribute("company", company.id);
    return new JwtGenerator(new SecretSignatureConfiguration(SecurityModule.SECRET)).generate(profile);
  }

  public boolean isAdmin(){
    return admin;
  }

}

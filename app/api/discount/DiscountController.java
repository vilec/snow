package api.discount;

import api.event.Event;
import api.event.EventService;
import com.google.inject.Inject;
import play.Logger;
import play.Play;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Result;

import java.io.File;

import static play.mvc.Results.ok;

public class DiscountController {

  public static final String MAILCHIMP_NEWSLETTER = "http://snowtribe.us16.list-manage.com/subscribe/post?u=bf6a0c5ef46612cc51dcc99ff&id=641fa41d16";
  @Inject
  FormFactory formFactory;

  @Inject
  MailerClient mailerClient;

  @Inject
  DiscountService discountService;

  @Inject
  EventService eventService;

  @Inject
  WSClient ws;

  @Transactional
  public Result discount() throws InterruptedException {
    Form<DiscountRequest> discountForm = formFactory.form(DiscountRequest.class);
    DiscountRequest request = discountForm.bindFromRequest().get();
    if (request.newsletter) {
      signUpNewsletter(request.email);
    }
    sendDiscountEmail(request);
    return ok();
  }

  private void sendDiscountEmail(DiscountRequest request) {
    Event event = eventService.find(request.eventId);
    String code = discountService.generateCode(request);
    String body = views.html.discountEmail.render(code, event, request.email).body();
    String emailImages = Play.application().configuration().getString("emailsPath");

    Email email = new Email()
      .setSubject("SnowTribe.pl - Kod zniżkowy o wartości " + event.company.discount + " PLN!")
      .setFrom("SnowTribe.pl <kontakt@snowtribe.pl>")
      .addTo(request.email)
      .addAttachment("image.jpg", new File(emailImages + "email_top.jpg"), "1")
      .addAttachment("image.jpg", new File(emailImages + "email_logo.png"), "2")
      .setBodyHtml(body);
    mailerClient.send(email);
  }

  private void signUpNewsletter(String email) {
    ws.url(MAILCHIMP_NEWSLETTER + "&subscribe=subscribe&EMAIL=" + email)
      .setFollowRedirects(false)
      .setContentType("application/x-www-form-urlencoded")
      .post("subscribe=subscribe&EMAIL=" + email)
      .thenApply((WSResponse r) -> {
        String title = r.getBody();
        Logger.info(title);
        return ok("Feed title: " + title);
      });

  }
}

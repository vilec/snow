package api.discount;

import com.google.inject.Inject;
import play.db.jpa.JPAApi;

public class DiscountRepository {

  @Inject
  private JPAApi jpaApi;

  public void store(DiscountEvent event) {
    jpaApi.em().merge(event);
  }

}

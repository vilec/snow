package api.discount;

public class DiscountRequest {
  public String email;
  public boolean newsletter;
  public int eventId;

  public void setEmail(String email) {
    this.email = email;
  }

  public void setNewsletter(boolean newsletter) {
    this.newsletter = newsletter;
  }

  public void setEventId(int eventId) {
    this.eventId = eventId;
  }
}

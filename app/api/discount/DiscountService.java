package api.discount;

import api.event.Event;
import api.event.EventRepository;
import com.google.inject.Inject;
import org.joda.time.DateTime;

public class DiscountService {

  @Inject
  EventRepository eventRepository;

  @Inject
  DiscountRepository discountRepository;

  public String generateCode(DiscountRequest request) {
    Event event = eventRepository.find(request.eventId);
    if (!event.company.partner) {
      throw new IllegalStateException("This company is not our partner!");
    }
    String code = generateDiscountCode(event.company.discountCode);
    storeEvent(request, event, code);
    return code;
  }

  private void storeEvent(DiscountRequest request, Event event, String code) {
    DiscountEvent discountEvent = new DiscountEvent();
    discountEvent.event = event;
    discountEvent.code = code;
    discountEvent.email = request.email;
    discountEvent.newsletter = request.newsletter;
    discountEvent.date = DateTime.now().toDate();
    discountRepository.store(discountEvent);
  }

  private String generateDiscountCode(String discountCode) {
    return discountCode;
  }

}

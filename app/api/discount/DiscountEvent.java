package api.discount;

import api.event.Event;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "discount_event")
public class DiscountEvent {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String email;
  public String code;
  public Date date;
  public boolean newsletter;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  @JsonIgnore
  public Event event;
}

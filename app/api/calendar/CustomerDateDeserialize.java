package api.calendar;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomerDateDeserialize extends JsonDeserializer<Date> {

  private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

  @Override
  public Date deserialize(JsonParser paramJsonParser, DeserializationContext paramDeserializationContext) throws IOException {
    String str = paramJsonParser.getText().trim();
    try {
      return dateFormat.parse(str);
    } catch (ParseException e) {

    }
    return paramDeserializationContext.parseDate(str);
  }
}

package api.calendar;


import api.event.Filter;

import java.time.LocalDate;
import api.event.Filter.Year;
import java.time.ZoneOffset;
import java.util.Date;

public class CalendarService {

  public Date getCurrentDate(){
    return Date.from(LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC));
  }

  public int getCurrentYear(){
    // TODO: quick fix - year hardcoded for winter season
    return 2017;
  }

  private Date convertToDate(String date, int year) {
    return java.sql.Date.valueOf(LocalDate.parse(year + "-" + date));
  }

  public Date getFromDate(Filter filter) {
     return convertToDate(filter.getFrom(), getCurrentYear() + ((Year) filter.getValue()).getFrom());
  }

  public Date getToDate(Filter filter) {
    return convertToDate(filter.getTo(), getCurrentYear() + ((Year) filter.getValue()).getTo());
  }
}

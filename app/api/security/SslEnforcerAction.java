package api.security;

import org.apache.commons.codec.binary.StringUtils;
import play.Logger;
import play.Play;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class SslEnforcerAction extends play.mvc.Action<SslEnforced> {

  @Override
  public CompletionStage<Result> call(Http.Context context) {
    String sslEnabled = Play.application().configuration().getString("app.ssl.enabled");
    if (!StringUtils.equals(sslEnabled, "true")) {
      return delegate.call(context);
    }

    if (context.request().secure()) {
      return delegate.call(context);
    }

    Logger.info("SSL Enforcer - Request not secured - Redirecting to HTTPS");

    String target = "";
    if (configuration.response() == SslEnforcedResponse.SELF) {
      target = "https://" + context.request().host() + context.request().uri();
    } else {
      target = "";
    }
    //if we are here then ssl is enabled and the request wasn't ssl, so reject them
    return F.Promise.pure(Controller.movedPermanently(target));
  }
}



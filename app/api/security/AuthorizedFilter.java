package api.security;

import com.google.inject.*;
import org.pac4j.play.filters.*;
import play.http.*;
import play.mvc.*;

public class AuthorizedFilter implements HttpFilters {

  @Inject
  private SecurityFilter securityFilter;

  @Override
  public EssentialFilter[] filters() {
    return new EssentialFilter[]{securityFilter.asJava()};
  }

}

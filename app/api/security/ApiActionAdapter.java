package api.security;

import org.pac4j.core.context.*;
import org.pac4j.play.*;
import org.pac4j.play.http.*;
import static play.libs.Json.toJson;
import play.mvc.*;
import static play.mvc.Results.forbidden;
import static play.mvc.Results.unauthorized;

public class ApiActionAdapter extends DefaultHttpActionAdapter {

    @Override
    public Result adapt(int code, PlayWebContext context) {
        if (code == HttpConstants.UNAUTHORIZED) {
            return unauthorized(toJson("denied"));
        } else if (code == HttpConstants.FORBIDDEN) {
            return forbidden(toJson("forbbiden")).as((HttpConstants.HTML_CONTENT_TYPE));
        } else {
            return super.adapt(code, context);
        }
    }
}

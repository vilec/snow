package api.security;

import com.google.inject.*;
import org.pac4j.core.authorization.authorizer.*;
import org.pac4j.core.client.*;
import org.pac4j.core.config.*;
import org.pac4j.core.context.*;
import org.pac4j.core.credentials.extractor.*;
import org.pac4j.http.client.direct.*;
import org.pac4j.jwt.config.encryption.*;
import org.pac4j.jwt.config.signature.*;
import org.pac4j.jwt.credentials.authenticator.*;
import org.pac4j.play.store.*;

public class SecurityModule extends AbstractModule {

  public final static String SECRET = "12345678901234567890123456789012";

  @Override
  protected void configure() {

    bind(PlaySessionStore.class).to(PlayCacheStore.class);

    JwtAuthenticator jwtAuthenticator = new JwtAuthenticator(new SecretSignatureConfiguration(SECRET), new SecretEncryptionConfiguration(SECRET));
    ParameterClient parameterClient = new ParameterClient("token", jwtAuthenticator);
    parameterClient.setSupportGetRequest(true);
    parameterClient.setCredentialsExtractor(new HeaderExtractor(HttpConstants.AUTHORIZATION_HEADER, "Bearer ", "bartek"));

    Clients clients = new Clients("http://localhost:9000/callback", parameterClient);

    Config config = new Config(clients);
    config.addAuthorizer("admin", new RequireAnyRoleAuthorizer<>("ROLE_ADMIN"));
    config.setHttpActionAdapter(new ApiActionAdapter());

    bind(Config.class).toInstance(config);
  }
}

package api;

import api.event.Event;
import api.event.EventService;
import api.security.SslEnforced;
import com.google.inject.Inject;
import play.data.*;
import play.db.jpa.*;
import play.mvc.*;
import views.html.*;

import java.util.List;
import java.util.stream.Collectors;

@SslEnforced
public class Application extends Controller {

  private final static String BASE_EVENT_URL = "https://snowtribe.pl/wyjazdy-zimowe-narty-snowboard/";

  @Inject
  private EventService eventService;

  public Application(){}

  public Application(FormFactory formFactory, JPAApi jpaApi) {

  }

  public Result app(String any) {
    return ok(app.render());
  }

  public Result redirectApp(String any) {
    return redirect("/app/");
  }

  public Result about() {
    return ok(about.render());
  }

  public Result events() {
    return ok(events.render());
  }

  public Result contact() {
    return ok(contact.render());
  }

  public Result landing(String subscribe) {
    return ok(landing.render(subscribe));
  }

  @Transactional
  public Result sitemap() {
    List<Event> allEvents = eventService.findAll();
    return ok(allEvents.stream().map(e -> BASE_EVENT_URL + e.link).collect(Collectors.joining("\n")));
  }

}

package api.resort;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class CountryEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String code;
  public String link;
  public String name;
  public String variant;
  public String description;
  public String flag;
}

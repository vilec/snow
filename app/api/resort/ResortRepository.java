package api.resort;

import api.event.Country;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class ResortRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Resort> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Resort> criteria = builder.createQuery(Resort.class);
    Root<Resort> resortRoot = criteria.from(Resort.class);
    criteria.select(resortRoot);
    return jpaApi.em().createQuery(criteria).getResultList();
  }

  public Resort find(long id) {
    return jpaApi.em().getReference(Resort.class, id);
  }

  public List<Resort> find(String link) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Resort> criteria = builder.createQuery(Resort.class);
    Root<Resort> eventRoot = criteria.from(Resort.class);
    criteria.select(eventRoot);
    criteria.where(builder.equal(eventRoot.get("link"), link));
    return jpaApi.em().createQuery(criteria)
      .getResultList();
  }

  public List<Resort> findByCountry(String countryLink) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Resort> criteria = builder.createQuery(Resort.class);
    Root<Resort> eventRoot = criteria.from(Resort.class);
    Join<Resort, CountryEntity> country = eventRoot.join("countryEntity");
    criteria.select(eventRoot);
    criteria.where(builder.equal(country.get("link"), countryLink));
    return jpaApi.em().createQuery(criteria)
      .getResultList();
  }

  public List<CountryEntity> findAllCountries() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<CountryEntity> criteria = builder.createQuery(CountryEntity.class);
    Root<CountryEntity> root = criteria.from(CountryEntity.class);
    criteria.select(root);
    return jpaApi.em().createQuery(criteria).getResultList();
  }
}

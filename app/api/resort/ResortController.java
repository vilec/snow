package api.resort;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class ResortController {

  @Inject
  private ResortService resortService;

  @Transactional
  public Result showResorts(String country) {
    List<Resort> resorts = resortService.findByCountry(country);
    return ok(views.html.resorts.render(resorts, resorts.get(0).countryEntity, resortService.findAllCountries()));
  }

  @Transactional
  public Result showAllResorts() {
    List<Resort> resorts = resortService.findAll();
    return ok(views.html.resorts.render(resorts, null, resortService.findAllCountries()));
  }

  @Transactional
  public Result listAll() {
    List<Resort> resorts = resortService.findAll();
    return ok(toJson(resorts));
  }

  @Transactional
  public Result showResort(String link) {
    Resort resort = resortService.find(link);
    return ok(views.html.resort.render(resort));
  }

}

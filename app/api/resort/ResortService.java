package api.resort;


import com.google.inject.Inject;

import java.util.List;

public class ResortService {

  @Inject
  private ResortRepository repository;

  public List<Resort> findAll() {
    return repository.findAll();
  }

  public Resort find(String link) {
    return repository.find(link).get(0);
  }

  public List<Resort> findByCountry(String country) {
    return repository.findByCountry(country);
  }

  public List<CountryEntity> findAllCountries() {
    return repository.findAllCountries();
  }
}

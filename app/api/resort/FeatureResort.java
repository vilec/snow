package api.resort;

import javax.persistence.*;

@Entity
@Table(name = "feature_resort")
public class FeatureResort {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public String code;
  public String name;
  public String icon;

}

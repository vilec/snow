package api.resort;

import api.event.Country;
import api.event.EventImage;
import api.event.feature.EventFeature;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

@Entity
public class Resort {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  public long id;

  public String name;

  @Enumerated(EnumType.STRING)
  public Country country;

  @ManyToOne
  @JoinColumn(name = "country_id")
  public CountryEntity countryEntity;

  @Column(name = "slope_length_easy")
  public String slopeLengthEasy;

  @Column(name = "slope_length_medium")
  public String slopeLengthMedium;

  @Column(name = "slope_length_hard")
  public String slopeLengthHard;

  @Column(name = "slope_length_expert")
  public String slopeLengthExpert;

  @OneToMany(mappedBy = "resort", cascade = CascadeType.ALL, orphanRemoval=true)
  public List<ResortImage> images;

  @OneToMany(mappedBy = "resort", cascade = CascadeType.ALL, orphanRemoval=true)
  public List<ResortFeature> features;

  public int length;

  public int altitude;

  public String link;
  public String description;

  @Column(name = "map_image")
  public String mapImage;

  public boolean freeride;
  public boolean snowpark;

  public String getCountryName() {
    return country.getName();
  }

  public String getFullName(){
    return name + ", " + getCountryName();
  }
}

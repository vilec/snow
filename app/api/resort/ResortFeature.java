package api.resort;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "resort_feature")
public class ResortFeature {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "resort_id", referencedColumnName = "id")
  @JsonIgnore
  public Resort resort;

  @ManyToOne
  @JoinColumn(name = "feature_resort_id")
  public FeatureResort feature;

  public boolean available;

  public boolean isAvailable() {
    return available;
  }
}

package api.resort;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "resort_image")
public class ResortImage {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "resort_id", referencedColumnName = "id")
  @JsonIgnore
  public Resort resort;

  public String path;
  public String name;
}

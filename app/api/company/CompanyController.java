package api.company;

import api.resort.Resort;
import api.resort.ResortService;
import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

public class CompanyController {

  @Inject
  private CompanyService companyService;

  @Transactional
  public Result listAll() throws InterruptedException {
    List<Company> companies = companyService.findAll();
    return ok(toJson(companies));
  }

}

package api.company;

import api.event.Event;
import api.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.List;

@Entity
public class Company {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  public boolean active;
  public String name;
  public String logo;
  public String url;
  public boolean partner;
  public Integer discount;

  @Column(name="price_xpath")
  public String priceXPath;

  @Column(name="discount_code")
  public String discountCode;

  @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  public List<Event> events;

  @OneToOne(mappedBy = "company")
  @JsonIgnore
  public User user;

  public String getName() {
    return name;
  }
}

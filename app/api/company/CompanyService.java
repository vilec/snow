package api.company;


import com.google.inject.Inject;

import java.util.List;

public class CompanyService {

  @Inject
  private CompanyRepository repository;

  public List<Company> findAll() {
    return repository.findAll();
  }
}

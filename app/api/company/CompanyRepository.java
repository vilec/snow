package api.company;

import api.resort.Resort;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CompanyRepository {

  @Inject
  private JPAApi jpaApi;

  public List<Company> findAll() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<Company> criteria = builder.createQuery(Company.class);
    Root<Company> root = criteria.from(Company.class);
    criteria.select(root);
    return jpaApi.em().createQuery(criteria).getResultList();
  }

  public Company find(long id){
    return jpaApi.em().getReference(Company.class, id);
  }
}

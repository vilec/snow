package api.monitoring;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

public class CheckAllTask {

  private final ActorRef checkAllActor;
  private final ActorSystem actorSystem;
  private final ExecutionContext executionContext;

  @Inject
  public CheckAllTask(@Named("check-all-actor") ActorRef checkAllActor, ActorSystem actorSystem, ExecutionContext executionContext) {
    this.checkAllActor = checkAllActor;
    this.actorSystem = actorSystem;
    this.executionContext = executionContext;

    this.initialize();
  }


  private void initialize() {
    actorSystem.scheduler().schedule(
      Duration.create(3600, TimeUnit.SECONDS), // initialDelay
      Duration.create(3600, TimeUnit.SECONDS), // interval
      checkAllActor,
      new CheckAllRequest(), // message,
      executionContext,
      ActorRef.noSender()
    );

  }
}

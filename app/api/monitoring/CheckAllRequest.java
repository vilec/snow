package api.monitoring;

public class CheckAllRequest {
  public Integer companyId;

  public CheckAllRequest(Integer companyId) {
    this.companyId = companyId;
  }

  public CheckAllRequest() {  }
}

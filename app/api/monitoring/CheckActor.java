package api.monitoring;

import akka.actor.UntypedActor;
import api.event.Event;
import api.monitoring.result.CheckResult;
import api.monitoring.result.CheckResultService;
import com.google.inject.Inject;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import play.Logger;
import play.db.jpa.JPAApi;

import static api.monitoring.result.CheckResult.Type.*;

public class CheckActor extends UntypedActor {

  @Inject
  private CheckResultService service;

  @Inject
  private JPAApi jpaApi;

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof CheckRequest) {
      CheckRequest request = (CheckRequest) message;
      Event event = request.event;

      Logger.info("Checking " + event.url);
      try {
        Document doc = Jsoup.connect(event.url).get();
        Elements newsHeadlines = doc.select(event.company.priceXPath);
        String priceString = newsHeadlines.get(0).ownText().replaceAll("\\D+", "");
        priceString = priceString.length() > 4 ? priceString.substring(0, 4) : priceString;
        int newPrice = Integer.parseInt(priceString);

        CheckResult result = new CheckResult(event);
        if (newPrice != event.price) {
          result.type = PRICE_DIFFERENCE;
          result.notes = "Cena SnowTribe: " + event.price + " | Cena " + event.company.name + ": " + newPrice;
        } else {
          result.type = OK;
        }

        jpaApi.withTransaction(() -> {
          service.store(result);
        });

        Logger.info(event.url + " | old price: " + event.price + " | current price: " + newPrice);
      } catch (Exception e) {
        CheckResult result = new CheckResult(event);
        result.type = NOT_FOUND;
        result.notes = "Błąd: " + e.getMessage();
        jpaApi.withTransaction(() -> service.store(result));
        Logger.error(event.url + " | ERROR: " + e.getMessage(), e);
      }
      sender().tell("Checked", sender());
    } else
      unhandled(message);
  }
}


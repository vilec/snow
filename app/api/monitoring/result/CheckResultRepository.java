package api.monitoring.result;


import api.event.Event;
import api.resort.Resort;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.stream.Collectors;

public class CheckResultRepository {

  @Inject
  private JPAApi jpaApi;

  public void store(CheckResult result) {
    jpaApi.em().persist(result);
    jpaApi.em().flush();
  }

  public List<CheckResult> findResults() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<CheckResult> criteria = builder.createQuery(CheckResult.class);
    Root<CheckResult> eventRoot = criteria.from(CheckResult.class);
    criteria.select(eventRoot);

    criteria.where(eventRoot.get("id").in(getLatestUpdateIds()));

    return jpaApi.em().createQuery(criteria).getResultList();
  }

  public CheckResult findLatestUpdate(Event event) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<CheckResult> criteria = builder.createQuery(CheckResult.class);
    Root<CheckResult> eventRoot = criteria.from(CheckResult.class);
    criteria.select(eventRoot);
    criteria.where(builder.equal(eventRoot.get("event"), event));
    criteria.orderBy(builder.desc(eventRoot.get("date")));

    List<CheckResult> results = jpaApi.em().createQuery(criteria).getResultList();
    return results.isEmpty() ? null : results.get(0);
  }

  public List<Long> getLatestUpdateIds() {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<CheckResult> criteria = builder.createQuery(CheckResult.class);
    Root<CheckResult> eventRoot = criteria.from(CheckResult.class);

    Join<CheckResult, Event> event = eventRoot.join("event");

    criteria.multiselect(event.get("id"), builder.max(eventRoot.get("id")));
    criteria.groupBy(event.get("id"));
    return jpaApi.em().createQuery(criteria).getResultList()
      .stream().map(u -> u.id).collect(Collectors.toList())
      ;
  }

  public CheckResult find(long resultId) {
    CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
    CriteriaQuery<CheckResult> criteria = builder.createQuery(CheckResult.class);
    Root<CheckResult> eventRoot = criteria.from(CheckResult.class);
    criteria.select(eventRoot);
    criteria.where(builder.equal(eventRoot.get("id"), resultId));
    return jpaApi.em().createQuery(criteria).getSingleResult();
  }
}

package api.monitoring.result;


import com.google.inject.Inject;
import play.Logger;
import play.Play;
import play.db.jpa.Transactional;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static api.monitoring.result.CheckResult.Type.OK;

public class CheckResultService {

  @Inject
  private CheckResultRepository repository;

  @Inject
  MailerClient mailerClient;

  @Transactional
  public void store(CheckResult result) {
    result.date = new Date();
    CheckResult latestUpdate = repository.findLatestUpdate(result.event);
    repository.store(result);
    if (result.type != OK) {
      if ((latestUpdate != null && latestUpdate.type == OK) || latestUpdate == null) {
        Logger.info("Sending email..");
        String hostURL = Play.application().configuration().getString("hostURL");
        String body = views.html.monitoringEmail.render(result, hostURL).body();
        Email email = new Email()
          .setSubject("SnowTribe.pl - Monitoring - Zmiana w ogłoszeniu "
            + result.event.company.name + " #" + result.event.id + "!")
          .setFrom("SnowTribe.pl <kontakt@snowtribe.pl>")
          .addTo("kontakt@snowtribe.pl")
          .setBodyHtml(body);
        mailerClient.send(email);
      }
    }
  }

  @Transactional
  public List<CheckResult> findResults() {
    List<CheckResult> results = repository.findResults();
    results.sort(Comparator.comparing(r -> r.type.priority));
    return results;
  }

  public CheckResult find(long resultId) {
    return repository.find(resultId);
  }
}

package api.monitoring.result;


import api.event.Event;
import net.sf.ehcache.store.CacheKeySet;

import javax.persistence.*;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Entity
@Table(name = "check_result")
public class CheckResult {

  private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm:ss");

  public CheckResult(Event event) {
    this.event = event;
  }

  public CheckResult() {}

  public CheckResult(long eventId, long checkResultId) {
    this.id = checkResultId;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  public Event event;

  public Type type;

  public Date date;
  public String notes;

  public enum Type {
     OK("OK", 10), PRICE_DIFFERENCE("CENA RÓŻNA", 2), NOT_FOUND("NIE ZNALEZIONO", 1);

    public Integer priority;
    private String description;

     Type(String description, int priority) {
       this.description = description;
       this.priority = priority;
     }
  }

  public String getDate() {
    return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(formatter);
  }

  public String getDescription() {
    return type.description;
  }

}

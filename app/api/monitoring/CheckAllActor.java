package api.monitoring;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import api.event.Event;
import api.event.EventService;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import play.Logger;
import play.db.jpa.JPAApi;

import java.util.List;
import java.util.stream.Collectors;

public class CheckAllActor extends UntypedActor {

  @Inject
  private EventService eventService;

  @Inject
  private JPAApi jpaApi;

  @Inject
  @Named("check-actor")
  private ActorRef checkActor;

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof CheckAllRequest) {
      CheckAllRequest request = (CheckAllRequest) message;
      if (request.companyId == null) {
        Logger.info("Checking all offers..");
      } else {
        Logger.info("Checking offers for company #" + request.companyId);
      }

      jpaApi.withTransaction(() -> {
          List<Event> events = eventService.findAll().stream()
            .filter(e -> e.company.priceXPath != null && e.active)
            .filter(e -> request.companyId == null || e.company.id == request.companyId)
            .collect(Collectors.toList());

          events.forEach(event -> checkActor.tell(new CheckRequest(event), self()));
          sender().tell("Scheduled checking " + events.size() + " offers", sender());
        }
      );
    } else
      unhandled(message);
  }
}


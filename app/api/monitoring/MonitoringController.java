package api.monitoring;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import api.event.Event;
import api.event.EventRepository;
import api.event.SearchResult;
import api.monitoring.result.CheckResult;
import api.monitoring.result.CheckResultService;
import api.security.SslEnforced;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletionStage;

import static akka.pattern.Patterns.ask;
import static play.libs.Json.toJson;

@SslEnforced
public class MonitoringController extends Controller {

  @Inject
  private CheckResultService service;

  @Inject
  @Named("check-all-actor")
  private ActorRef checkAllActor;

  public CompletionStage<Result> checkAll() {
    return FutureConverters.toJava(
      ask(checkAllActor, new CheckAllRequest(), 1000))
      .thenApply(response -> ok((String) response));
  }

  public CompletionStage<Result> checkCompany(Integer companyId) {
    return FutureConverters.toJava(
      ask(checkAllActor, new CheckAllRequest(companyId), 1000))
      .thenApply(response -> ok((String) response));
  }

  @Transactional
  public Result findResults() throws InterruptedException {
    List<CheckResult> results = service.findResults();
    return ok(toJson(results));
  }

  @Transactional
  public Result findResult(long resultId) {
    CheckResult result = service.find(resultId);
    return ok(toJson(result));
  }
}

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class UserService {
  private loggedIn = false;
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private http: Http, private router : Router) {
    this.loggedIn = !!this.getToken();
  }

  login(username : String, password : String) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.put('/api/login', JSON.stringify({ username, password }), { headers })
      .toPromise()
      .then(response => {
        if (response.json().token) {
          let user = this.jwtHelper.decodeToken(response.json().token);
          localStorage.setItem('id_token', response.json().token);
          localStorage.setItem('name', user.name);
          localStorage.setItem('logo', user.logo);
          localStorage.setItem('admin', user.admin);
          this.loggedIn = true;
          this.router.navigate(['/']);
          console.log(this.jwtHelper.decodeToken(response.json().token));
        }
      });
  }

  logout() {
   localStorage.removeItem('id_token');
   localStorage.removeItem('logo');
   localStorage.removeItem('name');
   localStorage.removeItem('admin');
   this.loggedIn = false;
   this.router.navigate(['/login']);
  }

  isLoggedIn() {
    this.loggedIn = !!this.getToken();
    return this.loggedIn;
  }

  getToken() {
    return localStorage.getItem('id_token');
  }

  getLogo(){
    return localStorage.getItem('logo');
  }

  getName(){
    return localStorage.getItem('name');
  }

  isAdmin(){
    return localStorage.getItem('admin') == "true";
  }
}

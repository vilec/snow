import {Component, OnInit} from "@angular/core";
import {UserService} from "./user.service";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'login-cmp',
  templateUrl: 'login.component.html',
  providers: [UserService],
})

export class LoginComponent implements OnInit  {

  username: String;
  password: String;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    if (this.userService.isLoggedIn()){
      this.router.navigate(['/'])
    }

    setTimeout(function(){
      $('.card').removeClass('card-hidden');
    }, 300)
  }


  login(): void {
    this.userService.login(this.username, this.password);
  }

}

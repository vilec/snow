import {Component, OnInit} from "@angular/core"
declare var $:JQueryStatic;

@Component({
  selector: 'my-app',
  moduleId: module.id,
  templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit {
  ngOnInit() {
  }

  public isRoute(path: string) {
    if (path === window.location.pathname) {
      return true
    }
    else {
      return false
    }
  }
}

import { Route } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {AuthGuard} from "./auth/auth.guard";
import {LoginComponent} from "./auth/login.component";
import { HomeComponent } from './dashboard/home/home.component';
import { UserComponent } from './dashboard/user/user.component';
import { EventsComponent } from './dashboard/event/events.component';
import { EventComponent } from './dashboard/event/event.component';
import {MonitoringComponent} from "./dashboard/monitoring/monitoring.component";
import {ResultComponent} from "./dashboard/monitoring/result.component";

export const APP_ROUTES: Route[] =[
    { path: '' , component: DashboardComponent, canActivate: [AuthGuard],
      children: [
        { path: '', redirectTo: 'events', pathMatch: 'full' },
        { path: 'dashboard', component: HomeComponent },
        { path: 'events', component: EventsComponent },
        { path: 'monitoring', component: MonitoringComponent },
        { path: 'monitoring/:id', component: ResultComponent },
        { path: 'event/:id', component: EventComponent },
        { path: 'event', component: EventComponent },
        { path: 'user', component: UserComponent },
      ]
    },
    { path: 'login', component: LoginComponent }
];

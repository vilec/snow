import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuard }   from '../auth/auth.guard';
import { UserService } from "../auth/user.service";
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { EventsComponent } from './event/events.component';
import { EventComponent } from './event/event.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileUploadModule } from "ng2-file-upload";
import {MonitoringComponent} from "./monitoring/monitoring.component";
import {ResultComponent} from "./monitoring/result.component";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FileUploadModule
    ],
    declarations: [
      HomeComponent,
      UserComponent,
      EventsComponent,
      EventComponent,
      MonitoringComponent,
      ResultComponent
    ],
    providers: [
      AuthGuard, UserService
    ]
})

export class DashboardModule{}

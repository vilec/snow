import {Injectable} from "@angular/core";
import {Headers} from "@angular/http";
import {AuthHttp} from "angular2-jwt";

import "rxjs/add/operator/toPromise";

import {CheckResult} from "./check-result";

@Injectable()
export class MonitoringService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private monitoringUrl = 'api/monitor/results';  // URL to web api
  private checkallUrl = 'api/monitor/check/all';  // URL to web api

  constructor(private authHttp: AuthHttp) {
  }

  getResults(): Promise<CheckResult[]> {
    return this.authHttp.get(this.monitoringUrl)
      .toPromise()
      .then(response => response.json() as CheckResult[])
      .catch(this.handleError);
  }

  getResult(id: number): Promise<CheckResult> {
    const url = `${this.monitoringUrl}/${id}`;
    return this.authHttp.get(url)
      .toPromise()
      .then(response => response.json() as CheckResult)
      .catch(this.handleError);
  }

  checkAll(): void {
    this.authHttp.get(this.checkallUrl)
      .toPromise()
      .then(() => response)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


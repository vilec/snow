import {Resort} from "./resort";
import {EventPrice} from "./event-price";
import {EventTransport} from "./event-transport";
import {EventAccommodation} from "app/assets/app/dashboard/event/event-accommodation";
import {EventFeature} from "./event-feature";
import {EventImage} from "./event-image";
import {Company} from "./company";
import {Category} from "./category";

export class CheckResult {
  id: number;
  date: string;
  type: string;
  notes: string;
  event: Event;
}


import {AfterViewChecked, Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

import "rxjs/add/operator/switchMap";

import {animate, state, style, transition, trigger} from "@angular/animations";
import {UserService} from "../../auth/user.service";
import {MonitoringService} from "./monitoring.service";
import {CheckResult} from "./check-result";


@Component({
  moduleId: module.id,
  selector: 'result-cmp',
  templateUrl: 'result.component.html',
  providers: [MonitoringService, UserService],
  animations: [
    trigger('table', [
      state('*', style({
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out')
      ])
    ])
  ]
})

export class ResultComponent implements OnInit, AfterViewChecked {

  result: CheckResult;

  constructor(private router: Router, private route: ActivatedRoute,
              private monitoringService: MonitoringService, private userService: UserService) {
    this.user = userService;
  }

  ngOnInit(): void {

    var resultId: number;
    this.route.params.subscribe(params => {
      resultId = +params["id"];
    });

    this.monitoringService.getResult(resultId)
      .then(result => this.result = result);
  }

  editEvent(event: Event): void {
    this.router.navigate(['/event', event.id]);
  }

}

import {Component, OnInit,AfterViewInit,trigger,state,style,transition,animate,keyframes,  ViewChildren, QueryList} from '@angular/core';
import { Router }            from '@angular/router';

import {UserService} from "../../auth/user.service";
import {MonitoringService} from "./monitoring.service";
import {CheckResult} from "./check-result";


@Component({
    moduleId: module.id,
    selector: 'monitoring-cmp',
    templateUrl: 'monitoring.component.html',
    providers: [MonitoringService],
    animations: [
        trigger('table', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0s ease-out')
                ])
        ])
    ]
})

export class MonitoringComponent implements OnInit, AfterViewInit {
  results: CheckResult[];
  user : UserService;

  @ViewChildren('resultList') resultChildren: QueryList<any>;

  constructor (private monitoringService: MonitoringService,  private router: Router, private userService: UserService) {
    this.user = userService;
  }

  checkAll(): void {
    this.monitoringService.checkAll();
  }

  getResults(): void {
    this.monitoringService
      .getResults()
      .then(e => {
        this.results = e;
      });
  }

  ngAfterViewInit() : void {
    this.resultChildren.changes.subscribe(e => {
      $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({ trigger: "hover" });
    })
  }

  ngOnInit(): void {
    this.getResults();
  }

  showResult(result: CheckResult): void {
    this.router.navigate(['/monitoring', result.id]);
  }

}


/*
 Copyright 2017 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

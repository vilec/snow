import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'dashboard-cmp',
    templateUrl: 'dashboard.component.html'
})

export class  DashboardComponent implements OnInit {
  ngOnInit() {
    $.getScript('../../assets/js/now-ui-kit.js');
  }

}

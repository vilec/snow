import { Injectable }    from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import {Resort} from "../event/resort";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ResortService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private resortsUrl = 'api/resorts';  // URL to web api

  constructor(private authHttp: AuthHttp) { }

  getResorts(): Promise<Resort[]> {
    return this.authHttp.get(this.resortsUrl)
      .toPromise()
      .then(response => response.json() as Resort[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


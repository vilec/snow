
import {Accommodation} from "./accommodation";

export class EventAccommodation{
  id: number;
  accommodation: Accommodation;
  offered: boolean;
}


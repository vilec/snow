
import {Transport} from "./transport";

export class EventTransport {
  id: number;
  transport: Transport;
  kind: string;
  value: number;
  currency: string;
}


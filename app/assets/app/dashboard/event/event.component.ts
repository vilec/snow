import {AfterViewChecked, Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

import {Resort} from "./resort";
import {Event} from "./event";

import {EventsService} from "./events.service";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

import "rxjs/add/operator/switchMap";

import {animate, state, style, transition, trigger} from "@angular/animations";
import {ResortService} from "../resort/resort.service";
import {EventPrice} from "./event-price";
import {Price} from "./price";
import {EventTransport} from "./event-transport";
import {Transport} from "./transport";
import {EventAccommodation} from "./event-accommodation";
import {Accommodation} from "./accommodation";
import {EventFeature} from "./event-feature";
import {Feature} from "./feature";

import {FileUploader} from "ng2-file-upload";
import {UserService} from "../../auth/user.service";
import {EventImage} from "./event-image";
import {Company} from "./company";
import {CategoryService} from "../category/category.service";
import {Category} from "./category";


declare var $:JQueryStatic;

@Component({
  moduleId: module.id,
  selector: 'event-cmp',
  templateUrl: 'event.component.html',
  providers: [EventsService, ResortService, CategoryService, UserService],
  animations: [
    trigger('table', [
      state('*', style({
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out')
      ])
    ])
  ]
})

export class EventComponent implements OnInit, AfterViewChecked {
  addMode: boolean;
  event: Event;
  resorts: Resort[];
  categories: Category[];
  companies: Company[];
  fb: FormBuilder;
  user: UserService;
  uploader : FileUploader;

  eventForm : FormGroup;

  constructor(private eventService: EventsService, private resortService: ResortService, private router : Router,
              private route: ActivatedRoute, private categoryService: CategoryService,
              fb: FormBuilder, private userService: UserService) {
    this.user = userService;
    this.fb = fb;
    this.eventForm = fb.group({
      id: [''],
      name: ['', Validators.compose([Validators.required, Validators.maxLength(31)])],
      url: ['', Validators.required],
      price: ['', Validators.compose([this.validateNumber, Validators.min(0), Validators.max(10000)])],
      priceFrom: [''],
      priceNotes: [''],
      start: ['', Validators.required],
      end: ['', Validators.required],
      people: ['', Validators.compose([this.validateNumber, Validators.min(0), Validators.max(10000)])],
      resort: ['', Validators.required],
      category: ['', Validators.required],
      company: ['', this.user.isAdmin() ? Validators.required : null],
      stars: ['', Validators.compose([this.validateNumber, Validators.min(1), Validators.max(5)])],
      rooms: ['', Validators.required],
      active: [''],
      transports: fb.array([]),
      prices: fb.array([]),
      accommodations: fb.array([]),
      features: fb.array([]),
      images: fb.array([])
    })
  }

  validateNumber(c: FormControl) {
  return c.value == "" || c.value == null || parseInt(c.value) ? null : {
    notanumber: {
      valid: false
    }
  };
}

  get prices(): FormArray {
    return this.eventForm.get('prices') as FormArray;
  };

  get transports(): FormArray {
    return this.eventForm.get('transports') as FormArray;
  };

  get accommodations(): FormArray {
    return this.eventForm.get('accommodations') as FormArray;
  };

  get features(): FormArray {
    return this.eventForm.get('features') as FormArray;
  };

  get images(): FormArray {
    return this.eventForm.get('images') as FormArray;
  };

  ngAfterViewChecked(): void {
    // HACK: Moving modals outside main-panel class,
    // because of position css it shows up under the faded background
    var modals = $('.modal').detach();
    $('body').append(modals);
  }

  ngOnInit(): void {

    this.uploader = new FileUploader({
      url: '/api/upload',
      authToken: "Bearer " + this.userService.getToken()
    });

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var file = JSON.parse(response);
      console.log(response, file);
      this.addImage(file.originalFileName, file.realFileName);
    };

    this.resortService.getResorts()
      .then(r => {
        this.resorts = r;
      });

    this.categoryService.getCategoires()
      .then(r => {
        this.categories = r;
      });

    this.eventService.getCompanies()
      .then(c => {
        this.companies = c;
      });

    var eventId: number;
    this.route.params.subscribe(params => {
      eventId = +params["id"];
    });

    if (eventId) {
      this.eventService.getEvent(eventId)
        .then(event => {
          this.event = event;
          this.eventForm.setValue({
            id: event.id,
            name: event.name,
            url: event.url,
            price: event.price,
            priceFrom: event.priceFrom,
            priceNotes: event.priceNotes,
            start: event.start,
            end: event.end,
            people: event.people,
            resort: event.resort.id,
            category: event.category.id,
            company: event.company.id,
            stars: event.stars,
            rooms: event.rooms,
            active: event.active,
            transports: [],
            prices: [],
            accommodations: [],
            features: [],
            images: []
          });
          this.createPrices(event.prices);
          this.createTransports(event.transports);
          this.createAccommodations(event.accommodations);
          this.createFeatures(event.features);
          this.createImages(event.images);
        });
    } else {
      this.addMode = true;
      this.createPrices([]);
      this.createTransports([]);
      this.createAccommodations([]);
      this.createFeatures([]);
      this.createImages([]);
    }
  }

  copyEvent() : void {
    this.eventService.copyEvent(this.event).then((e) => {
      this.router.navigate(['/event', e.id]);
      $.notify({
        icon: "now-ui-icons ui-2_like",
        message: "<strong>Brawo!</strong> Kopia wyjazdu została utworzona i <strong>zostałeś do niej przeniesiony</strong>!"
      }, {
        type: 'success'
      });
    });
  }

  createImages(images: EventImage[]) : void {
    const imageFormGroups = images.map(image => this.fb.group({
      id : [image.id],
      path : [image.path],
      name : [image.name],
      checked : true,
    }));
    this.eventForm.setControl('images', this.fb.array(imageFormGroups));
  }

  createPrices(prices: EventPrice[]) : void {
    this.eventService.getPrices()
      .then(all => {
        const pricesFormGroups = all.map(price => {
          let storedPrice =  prices.find(p => p.price.id == price.id);
          return this.fb.group({
            id : [storedPrice != null ? storedPrice.id : ''],
            kind : [storedPrice != null ? storedPrice.kind : 'N_A'],
            value : [storedPrice != null ? storedPrice.value : ''],
            currency : [storedPrice != null ? storedPrice.currency : ''],
            description : [storedPrice != null ? storedPrice.description : ''],
            priceName: [price.name],
            priceTypeId: [price.id],
          })});
        this.eventForm.setControl('prices', this.fb.array(pricesFormGroups));
      });
  }

  createAccommodations(accommodations: EventAccommodation[]) : void {
    this.eventService.getAccommodations()
      .then(all => {
        const accommodationFormGroups = all.map(accommodation => {
          let storedAccomodation =  accommodations.find(a => a.accommodation.id == accommodation.id);
          return this.fb.group({
            id: [storedAccomodation != null ? storedAccomodation.id : ''],
            name: [accommodation.name],
            accommodationId: [accommodation.id],
            offered: [storedAccomodation != null ? storedAccomodation.offered : false],
          })
        });
        this.eventForm.setControl('accommodations', this.fb.array(accommodationFormGroups));
      });
  }

  createTransports(transports: EventTransport[]) : void {
    this.eventService.getTransports()
      .then(t => {
        const transportFormGroups = t.map(transport => {
          let storedTransport = transports.find(t => t.transport.id == transport.id);
          return this.fb.group({
            transportId: [transport.id],
            name: [transport.name],
            kind: [storedTransport != null ? storedTransport.kind : 'INCLUDED'],
            value: [storedTransport != null ? storedTransport.value : ''],
            currency: [storedTransport != null ? storedTransport.currency : ''],
            checked: [transports.findIndex(t => t.transport.id == transport.id) > -1],
          })
        });
        this.eventForm.setControl('transports', this.fb.array(transportFormGroups));
      });
  }

  createFeatures(features: EventFeature[]) : void {
    this.eventService.getFeatures()
      .then(f => {
        const featureFormGroup = f.map(feature => this.fb.group({
          featureId : [feature.id],
          name : [feature.name],
          checked : [features.findIndex(f => f.feature.id == feature.id) > -1],
        }));
        this.eventForm.setControl('features', this.fb.array(featureFormGroup));
      });
  }

  addImage(originalFileName: string, realFileName: string) {
    this.images.push(this.fb.group({
      id : [null],
      path : [realFileName],
      name : [originalFileName],
      checked : true,
    }));
    this.images.markAsDirty();
  }

  onSubmit() : void {
    const formModel = this.eventForm.value;

    const pricesCopy: EventPrice[] = formModel.prices.map(
      (price: any) => {
        let result = Object.assign({}, price);
        let p = new Price();
        p.name = price.priceName;
        p.id = price.priceTypeId;
        result.price = p;
        return result;
      }
    );

    const transportsCopy: EventTransport[] = formModel.transports
      .filter((t : any) => t.checked)
      .map((transport: any) => {
        let result = new EventTransport();
        result.kind = transport.kind;
        result.value = transport.value;
        result.currency = transport.currency;
        let t = new Transport();
        t.id = transport.transportId;
        t.transportType = transport.transportType;
        result.transport = t;
        return result;
    }
    );

    const imagesCopy: EventImage[] = formModel.images
      .filter((i : any) => i.checked)
      .map((image: any) => {
          let newImage = new EventImage();
          newImage.id = image.id;
          newImage.path = image.path;
          newImage.name = image.name;
          return newImage;
        }
      );

    const featuresCopy: EventFeature[] = formModel.features
      .filter((f : any) => f.checked)
      .map((feature: any) => {
        let result = new EventFeature();
        let f = new Feature();
        f.id = feature.featureId;
        result.feature = f;
        return result;
    }
    );

    const accommodationsCopy: EventAccommodation[] = formModel.accommodations
      .map((accommodation: any) => {
          let result = Object.assign({}, accommodation);
          let a = new Accommodation();
          a.id = accommodation.accommodationId;
          result.accommodation = a;
          return result;
        }
      );

    const event: Event = {
      id: this.event ? this.event.id : null,
      name: formModel.name,
      start: formModel.start,
      url: formModel.url,
      end: formModel.end,
      price: formModel.price,
      priceFrom: formModel.priceFrom,
      priceNotes: formModel.priceNotes,
      people: formModel.people,
      rooms: formModel.rooms,
      stars: formModel.stars,
      active: formModel.active,
      resort: {
        id: formModel.resort,
        fullName: '',
      },
      category: {
        id: formModel.category,
        name: '',
      },
      company: {
        id: formModel.company,
        name: '',
      },
      prices: pricesCopy,
      transports: transportsCopy,
      accommodations: accommodationsCopy,
      features: featuresCopy,
      images: imagesCopy
    };
    this.eventService.update(event)
      .then(() => {
        this.router.navigate(['/events']);
        $.notify({
          icon: "now-ui-icons ui-2_like",
          message: "<strong>Brawo!</strong> Oferta wyjazdu została zapisana"
        },{
          type: 'success'
        });
      });
  }

}

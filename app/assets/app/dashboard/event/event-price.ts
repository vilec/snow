import {Price} from "./price";

export class EventPrice {
  id: number;
  price: Price;
  value: number;
  currency: string;
  description: string;
  kind: string;
}


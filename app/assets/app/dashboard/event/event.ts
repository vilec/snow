import {Resort} from "./resort";
import {EventPrice} from "./event-price";
import {EventTransport} from "./event-transport";
import {EventAccommodation} from "app/assets/app/dashboard/event/event-accommodation";
import {EventFeature} from "./event-feature";
import {EventImage} from "./event-image";
import {Company} from "./company";
import {Category} from "./category";

export class Event {
  id: number;
  active: boolean;
  name: string;
  url: string;
  start: string;
  end: string;
  price: number;
  priceFrom: boolean;
  priceNotes: string;
  people: number;
  stars: number;
  rooms: string;
  notes: string;

  resort: Resort;
  company: Company;
  category: Category;

  prices: EventPrice[];
  transports: EventTransport[];
  accommodations: EventAccommodation[];
  features: EventFeature[];
  images: EventImage[];

}


import {Injectable} from "@angular/core";
import {Headers} from "@angular/http";
import {AuthHttp} from "angular2-jwt";

import "rxjs/add/operator/toPromise";

import {Event} from "./event";
import {Price} from "./price";
import {EventTransport} from "./event-transport";
import {Transport} from "app/assets/app/dashboard/event/transport";
import {Accommodation} from "./accommodation";
import {Feature} from "./feature";
import {Company} from "./company";

@Injectable()
export class EventsService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private eventsUrl = 'api/events';  // URL to web api
  private pricesUrl = 'api/prices';  // URL to web api
  private transportsUrl = 'api/transports';  // URL to web api
  private featuresUrl = 'api/features';  // URL to web api
  private accommodationsUrl = 'api/accommodations';  // URL to web api
  private eventUrl = 'api/event';  // URL to web api
  private eventCopyUrl = 'api/event/copy/';  // URL to web api
  private companiesUrl = 'api/companies';  // URL to web api

  constructor(private authHttp: AuthHttp) {
  }

  getEvents(): Promise<Event[]> {
    return this.authHttp.get(this.eventsUrl)
      .toPromise()
      .then(response => response.json() as Event[])
      .catch(this.handleError);
  }

  getPrices(): Promise<Price[]> {
    return this.authHttp.get(this.pricesUrl)
      .toPromise()
      .then(response => response.json() as Price[])
      .catch(this.handleError);
  }

  getCompanies(): Promise<Company[]> {
    return this.authHttp.get(this.companiesUrl)
      .toPromise()
      .then(response => response.json() as Company[])
      .catch(this.handleError);
  }

  getTransports(): Promise<Transport[]> {
    return this.authHttp.get(this.transportsUrl)
      .toPromise()
      .then(response => response.json() as Transport[])
      .catch(this.handleError);
  }
  getFeatures(): Promise<Feature[]> {
    return this.authHttp.get(this.featuresUrl)
      .toPromise()
      .then(response => response.json() as Feature[])
      .catch(this.handleError);
  }

  getAccommodations(): Promise<Accommodation[]> {
    return this.authHttp.get(this.accommodationsUrl)
      .toPromise()
      .then(response => response.json() as Accommodation[])
      .catch(this.handleError);
  }

  update(event: Event): Promise<Event> {
    const url = `${this.eventUrl}`;
    return this.authHttp
      .put(url, JSON.stringify(event), {headers: this.headers})
      .toPromise()
      .then(() => event)
      .catch(this.handleError);
  }

  copyEvent(event: Event): Promise<Event> {
    const url = `${this.eventCopyUrl + event.id}`;
    return this.authHttp
      .get(url)
      .toPromise()
      .then(response => response.json() as Event)
      .catch(this.handleError);
  }

  create(beacon: Event): Promise<Event> {
    const url = `${this.eventUrl}`;
    return this.authHttp
      .post(url, JSON.stringify(beacon), {headers: this.headers})
      .toPromise()
      .then(() => beacon)
      .catch(this.handleError);
  }

  getEvent(id: number): Promise<Event> {
    const url = `${this.eventUrl}/${id}`;
    return this.authHttp.get(url)
      .toPromise()
      .then(response => response.json() as Event)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


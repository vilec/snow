import {Component, OnInit,AfterViewInit,trigger,state,style,transition,animate,keyframes} from '@angular/core';
import { Router }            from '@angular/router';

import { Event }                from './event';
import { EventsService }         from './events.service';
import {UserService} from "../../auth/user.service";


@Component({
    moduleId: module.id,
    selector: 'events-cmp',
    templateUrl: 'events.component.html',
    providers: [EventsService],
    animations: [
        trigger('table', [
            state('*', style({
                '-ms-transform': 'translate3D(0px, 0px, 0px)',
                '-webkit-transform': 'translate3D(0px, 0px, 0px)',
                '-moz-transform': 'translate3D(0px, 0px, 0px)',
                '-o-transform':'translate3D(0px, 0px, 0px)',
                transform:'translate3D(0px, 0px, 0px)',
                opacity: 1})),
                transition('void => *', [
                    style({opacity: 0,
                        '-ms-transform': 'translate3D(0px, 150px, 0px)',
                        '-webkit-transform': 'translate3D(0px, 150px, 0px)',
                        '-moz-transform': 'translate3D(0px, 150px, 0px)',
                        '-o-transform':'translate3D(0px, 150px, 0px)',
                        transform:'translate3D(0px, 150px, 0px)',
                    }),
                    animate('0.3s 0s ease-out')
                ])
        ])
    ]
})

export class EventsComponent implements OnInit {
  events: Event[];
  user : UserService;

  constructor (private eventService: EventsService,  private router: Router, private userService: UserService) {
    this.user = userService;
  }

  getEvents(): void {
    this.eventService
      .getEvents()
      .then(e => this.events = e);
  }

  addEvent(): void {
    this.router.navigate(['/event']);
  }

  showEvent(event: Event): void {
    this.router.navigate(['/event', event.id]);
  }

  ngOnInit(): void {
    this.getEvents();
  }

}


/*
 Copyright 2017 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

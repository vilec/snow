import { Injectable }    from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import {Resort} from "../event/resort";

import 'rxjs/add/operator/toPromise';
import {Category} from "../event/category";

@Injectable()
export class CategoryService {

  private resortsUrl = 'api/categories';  // URL to web api

  constructor(private authHttp: AuthHttp) { }

  getCategoires(): Promise<Category[]> {
    return this.authHttp.get(this.resortsUrl)
      .toPromise()
      .then(response => response.json() as Category[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


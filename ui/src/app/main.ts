import { platformBrowserDynamic }  from '@angular/platform-browser-dynamic';
import { EventsModule } from './events.module';
import { enableProdMode } from '@angular/core';
import { environment } from '../environments/environment';

if (environment.production) {
  enableProdMode();
}
platformBrowserDynamic().bootstrapModule(EventsModule)
  .catch(err => console.log(err));

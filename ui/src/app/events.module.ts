
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { EventsComponent} from './events/events.component';
import {FilterComponent} from "./filter/filter.component";
import {AppComponent} from "./app.component";
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { RouterModule } from '@angular/router';
import {APP_ROUTES} from "./events.routes";
import {APP_BASE_HREF} from "@angular/common";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InfiniteScrollModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: '/' + (window.location.pathname.split('/')[1] || '')
    }
  ],
  declarations: [
    EventsComponent,
    FilterComponent,
    AppComponent
  ],
  bootstrap: [ AppComponent ],
})
export class EventsModule { }

/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

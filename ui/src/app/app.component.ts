import {Component, ElementRef, Input, OnInit}          from '@angular/core';
import {EventService} from "./events/event.service";
import * as $ from 'jquery';

@Component({
 // moduleId: module.id,
  selector: 'events-app',
  templateUrl: 'app.component.html',
  providers: [EventService],

})
export class AppComponent implements OnInit  {

  constructor (private elementRef:ElementRef) {
    this.query = this.elementRef.nativeElement.getAttribute('static-query');
  }

  query:string;

  ngOnInit() {
    console.log('static query: ' + this.query);
    $.getScript('../../assets/js/svg.js');
  }
}

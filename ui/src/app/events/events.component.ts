import { Component, OnInit, OnChanges, AfterViewInit, ViewChildren, QueryList}          from '@angular/core';
import {EventService} from "./event.service";
import { Event } from './event';
import {ActivatedRoute} from "@angular/router";
declare var $: any;

@Component({
  //moduleId: module.id,
  selector: 'events-list',
  templateUrl: 'events.component.html',
  providers: []
})
export class EventsComponent implements OnInit, OnChanges, AfterViewInit  {

  @ViewChildren('eventsList') eventsChildren: QueryList<any>;

  events: Event[];
  totalEvents: number = 0;
  refreshing: boolean = false;
  currentPage: number = 1;
  currentFilters: string[] = [];
  loadingMore : boolean = false;
  allLoaded : boolean = false;
  noResults : boolean = false;

  scrollDistance: number = 2;
  throttle: number = 300;
  eventsPerPage: number = 10;

  constructor (private eventService: EventService, private route: ActivatedRoute) {
  }

  loadMoreEvents () {
    if (this.loadingMore || this.allLoaded || this.noResults || this.refreshing) {
      return;
    }
    this.loadingMore = true;
    this.currentPage++;

    this.eventService
      .loadEvents(this.currentFilters, this.currentPage)
      .then(b => {
        if (b.events.length != this.eventsPerPage){
          this.allLoaded = true;
        }
        this.events = this.events.concat(b.events);
        this.loadingMore = false;
      });
  }

  refreshFilters(filters : string[]) : void {
    this.currentPage = 1;
    this.allLoaded = false;
    this.noResults = false;
    this.refreshing = true;
    this.currentFilters = filters;
    this.eventService
      .loadEvents(filters)
      .then(b => {
        if (b.events.length == 0){
          this.noResults = true;
        }
        this.events = b.events;
        this.totalEvents = b.total;
        this.refreshing = false;
      });

  }

  ngOnChanges() : void {
    $.getScript('../../assets/js/svg.js');
  }

  ngAfterViewInit() : void {
    this.eventsChildren.changes.subscribe(e => {
      $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({ trigger: "hover" });
    })
  }

  ngOnInit(): void {
    this.eventService.setEventsComponentRef(this);
  }
}

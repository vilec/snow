import {Event} from "./event";

export class SearchResult {
  events: Event[];
  total: number;
}


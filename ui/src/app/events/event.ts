export class Event {
  id: number;
  name: string;
  start: string;
  end: string;
  price: number;
  transport: string;
  resort : {
    id : number;
    name: string;
    country: string;
  };
  company: {
    partner: boolean;
    logo: string;
  }
}


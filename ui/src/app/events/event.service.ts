import { Injectable, EventEmitter }    from '@angular/core';
import { Http, Response }          from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Event } from './event';
import {EventsComponent} from "./events.component";
import {SearchResult} from "./search-result";

@Injectable()
export class EventService {

  private eventURL = 'api/search';  // URL to web api
  private events: EventsComponent;

  constructor(private http: Http) {
  }

  refreshComponent(filters: string[]) : void {
    this.events.refreshFilters(filters);
  }

  loadEvents(filters: string[], page = 1): Promise<SearchResult> {
    return this.http.get(this.eventURL + '?query=' + filters.join().toLowerCase() + "&page=" + page)
      .toPromise()
      .then(response => response.json() as SearchResult)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  public setEventsComponentRef(events: EventsComponent) : void {
    this.events = events;
  }
}


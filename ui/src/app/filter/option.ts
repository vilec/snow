export class Option {
  name: string;
  code: string;
  image: string;
  active : boolean;
  all : boolean;

  constructor(code: string, name: string, image?: string, active = false) {
    this.name = name;
    this.code = code;
    this.image = image;
    this.active = active;
    this.all = false;
  }

  setAll(all : boolean) : void {
    this.all = all;
  }

}


import {Option} from "./option";

export class Filter {
  name: string;
  options: Option[];
  features: boolean;
  hidden: boolean;

  constructor(name: string, features = false, hidden = false) {
    this.name = name;
    this.options = [];
    this.features = features;
    this.hidden = hidden;
  }

  selectOnlyAll(){
    this.options.forEach(o => o.active = o.all);
  }

  deselectAll(){
    this.options.filter(o => o.all)[0].active = false;
  }

  noActive() : boolean{
    return this.options.filter(o => o.active === true).length == 0;
  }

  anyActiveExceptAll() : boolean{
    return this.options.filter(o => o.all === false && o.active === true).length > 0;
  }

  addOption(option : Option){
    if (this.options.length == 0){
       option.setAll(true);
    }
    this.options.push(option);
  }
}


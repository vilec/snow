import {Component, Input, OnInit} from "@angular/core";
import {Filters} from "./filters";
import {Filter} from "./filter";
import {Option} from "./option";
import {EventService} from "../events/event.service";
import {Location} from "@angular/common";
import {ActivatedRoute, Event, Router, RoutesRecognized} from "@angular/router";
declare var ga: any;
declare var $: any;

@Component({
  //moduleId: module.id,
  selector: 'events-filter',
  templateUrl: 'filter.component.html',
  providers: [],

})
export class FilterComponent implements OnInit {

  filters: Filters;
  activeFilters: string[];
  activeFiltersDisplay: string[] = [];
  criteriaVisible: boolean;
  @Input() query: string;

  constructor(private eventService: EventService, private route: ActivatedRoute, private router: Router,
              private location: Location) {
  }

  public parseURLQuery(): void {
    this.router.events.subscribe((event: Event) => {
      if (!!event && event instanceof RoutesRecognized) {
        const params = event.state.root.firstChild.queryParams;
        let query = params["query"];

        if (this.query != null) {
          console.log("Static query: " + this.query);
          query = this.query;
        }

        if (query != null) {
          let filters = query.split(",");
          this.filters.filters.forEach(filter => {
            filter.options.forEach(option => {
              option.active = filters.indexOf(option.code.toLowerCase()) > -1;
            });
            if (!filter.anyActiveExceptAll()) {
              filter.selectOnlyAll();
            }
          });
          this.filters.sort.forEach(sort => {
            sort.active = filters.indexOf(sort.code.toLowerCase()) > -1;
          });
          if (!this.filters.anySortSelected()) {
            this.filters.selectDefault();
          }
        }
        this.refreshActiveFilters();
      }
    });
  }

  public refreshActiveFilters(): void {
    this.activeFilters = [];
    this.activeFiltersDisplay = [];

    for (let filter of this.filters.filters) {
      this.activeFilters = this.activeFilters.concat(filter.options
        .filter(f => f.active === true && !f.all)
        .map(f => f.code));

      this.activeFiltersDisplay = this.activeFiltersDisplay.concat(filter.options
        .filter(f => f.active === true && !f.all)
        .map(f => f.name));
    }

    this.activeFilters = this.activeFilters.concat(this.filters.sort
      .filter(f => f.active === true)
      .map(f => f.code));

    this.location.go('/?query=' + this.activeFilters.join(',').toLowerCase());
    //this.router.navigate(['/'], { queryParams: { query : this.activeFilters.join(',') }});
    ga('send', {'hitType': 'pageview', 'page': window.location.search, 'title': 'Search criteria'});
    this.eventService.refreshComponent(this.activeFilters);
  }

  public toggleCriteria(): void {
    this.criteriaVisible = !this.criteriaVisible;
    $('#filter-criteria').collapse('toggle');
  }

  public sortClicked(sort: Option): void {
    if (!sort.active) {
      this.filters.selectSort(sort);
    }
    this.refreshActiveFilters();
  }

  public optionClicked(filter: Filter, option: Option): void {
    if (option.all) {
      this.allOptionClicked(option, filter);
    } else {
      this.nonAllOptionClicked(option, filter);
    }
    this.refreshActiveFilters();
  }

  private nonAllOptionClicked(option: Option, filter: Filter) {
    option.active = !option.active;
    if (filter.noActive()) {
      filter.selectOnlyAll();
    } else if (filter.anyActiveExceptAll()) {
      filter.deselectAll();
    }
  }

  private allOptionClicked(option: Option, filter: Filter) {
    if (!option.active) {
      filter.selectOnlyAll();
    }
  }

  private checkIfCriteriaVisible() {
    this.router.events.subscribe((event: Event) => {
      if (!!event && event instanceof RoutesRecognized) {
        const params = event.state.root.firstChild.queryParams;
        let hide = params["hide"];
        if ($(window).width() >= 992 && hide == null) {
          $('#filter-criteria').addClass('show');
          this.criteriaVisible = true;
        }
      }
    });
  }

  ngOnInit(): void {
    this.checkIfCriteriaVisible();

    this.filters = new Filters();
    let term = new Filter('Termin');
    term.addOption(new Option('ALL_TERMS', 'Wszystkie', '', true));
    term.addOption(new Option('DECEMBER', 'Grudzień'));
    term.addOption(new Option('NEW_YEAR', 'Sylwester'));
    term.addOption(new Option('JANUARY', 'Styczeń'));
    term.addOption(new Option('FEBRUARY', 'Luty'));
    term.addOption(new Option('MARCH', 'Marzec'));
    term.addOption(new Option('APRIL', 'Kwiecień'));
    term.addOption(new Option('MAY', 'Maj'));
    this.filters.addFilter(term);

    let country = new Filter('Kraj');
    country.addOption(new Option('ALL_COUNTRIES', 'Wszystkie', '', true));
    country.addOption(new Option('FRANCE', 'Francja', 'countries/195-france.svg'));
    country.addOption(new Option('AUSTRIA', 'Austria', 'countries/003-austria.svg'));
    country.addOption(new Option('POLAND', 'Polska', 'countries/211-poland.svg'));
    country.addOption(new Option('ITALY', 'Włochy', 'countries/013-italy.svg'));
    country.addOption(new Option('GEORGIA', 'Gruzja', 'countries/256-georgia.svg'));
    country.addOption(new Option('CZECH', 'Czechy', 'countries/149-czech-republic.svg'));
    country.addOption(new Option('SWITZERLAND', 'Szwajcaria', 'countries/205-switzerland.svg'));
    this.filters.addFilter(country);

    let transport = new Filter('Transport');
    transport.addOption(new Option('ALL_TRANSPORTS', 'Wszystkie', '', true));
    transport.addOption(new Option('CAR', 'Dojazd własny', 'car.svg'));
    transport.addOption(new Option('BUS', 'Autobus', 'bus.svg'));
    transport.addOption(new Option('PLANE', 'Samolot', 'airplane.svg'));
    this.filters.addFilter(transport);

    let price = new Filter('Cena');
    price.addOption(new Option('ALL_PRICES', 'Wszystkie', '', true));
    price.addOption(new Option('LESS_900', '<900PLN'));
    price.addOption(new Option('BETWEEN_900_1200', '900-1200PLN'));
    price.addOption(new Option('BETWEEN_1200_1500', '1200-1500PLN'));
    price.addOption(new Option('BETWEEN_1500_2000', '1500-2000PLN'));
    price.addOption(new Option('BETWEEN_2000_2500', '2000-2500PLN'));
    price.addOption(new Option('MORE_2500', '>2500PLN'));
    this.filters.addFilter(price);

    let features = new Filter('Atrakcje', true);
    features.addOption(new Option('ALL_FEATURES', 'Wszystkie', '', true));
    features.addOption(new Option('FEATURE_BOARD_GAMES', 'Gry planszowe', 'horse.svg'));
    features.addOption(new Option('FEATURE_WORKSHOPS', 'Warsztaty', 'light-bulb.svg'));
    features.addOption(new Option('FEATURE_ICERINK', 'Lodowisko', 'ice-skating.svg'));
    features.addOption(new Option('FEATURE_SPA', 'Spa', 'spa.svg'));
    features.addOption(new Option('FEATURE_PARTY', 'Imprezy', 'confetti.svg'));
    features.addOption(new Option('FEATURE_TOURNAMENTS', 'Konkursy', 'cup.svg'));
    features.addOption(new Option('FEATURE_MUSIC', 'DJ / Muzyka na żywo', 'musical-note.svg'));
    features.addOption(new Option('FEATURE_EQUIPMENT_TESTS', 'Testy sprzętu', 'ski.svg'));
    features.addOption(new Option('FEATURE_PARAGLIDER', 'Lot na paralotni', 'parachute.svg'));
    features.addOption(new Option('FEATURE_APRES_SKI', 'Apres SKI', 'beer.svg'));
    features.addOption(new Option('FEATURE_TRAINING', 'Szkolenie narciarskie', 'snowboarder.svg'));
    features.addOption(new Option('FEATURE_GOPRO', 'Wypożyczenie GoPro', 'gopro.svg'));
    features.addOption(new Option('FEATURE_POKER', 'Turniej Pokera', 'poker.svg'));
    // commented out for now
    //this.filters.addFilter(features);

    let categories = new Filter('Rodzaj');
    categories.addOption(new Option('ALL_CATEGORIES', 'Wszystkie', '', true));
    categories.addOption(new Option('ADULT', 'Studenci i dorośli', 'chalet.svg'));
    categories.addOption(new Option('FAMILY', 'Rodzinne', 'family.svg'));
    categories.addOption(new Option('CHILDREN', 'Dla dzieci', 'children.svg'));
    categories.addOption(new Option('FREERIDE', 'Freeride', 'mountain.svg'));
    categories.addOption(new Option('TRAINING', 'Szkolenia', 'snowboarder.svg'));
    this.filters.addFilter(categories);

    let resorts = new Filter('Resort', false, true);
    resorts.addOption(new Option('ALL_RESORTS', 'Wszystkie', '', true));
    resorts.addOption(new Option('RESORT_1', '', ''));
    resorts.addOption(new Option('RESORT_2', '', ''));
    resorts.addOption(new Option('RESORT_3', '', ''));
    resorts.addOption(new Option('RESORT_4', '', ''));
    resorts.addOption(new Option('RESORT_5', '', ''));
    resorts.addOption(new Option('RESORT_6', '', ''));
    resorts.addOption(new Option('RESORT_7', '', ''));
    resorts.addOption(new Option('RESORT_8', '', ''));
    resorts.addOption(new Option('RESORT_9', '', ''));
    resorts.addOption(new Option('RESORT_10', '', ''));
    resorts.addOption(new Option('RESORT_11', '', ''));
    resorts.addOption(new Option('RESORT_12', '', ''));
    resorts.addOption(new Option('RESORT_13', '', ''));
    resorts.addOption(new Option('RESORT_14', '', ''));
    resorts.addOption(new Option('RESORT_15', '', ''));
    resorts.addOption(new Option('RESORT_16', '', ''));
    resorts.addOption(new Option('RESORT_17', '', ''));
    resorts.addOption(new Option('RESORT_18', '', ''));
    resorts.addOption(new Option('RESORT_19', '', ''));
    resorts.addOption(new Option('RESORT_20', '', ''));
    resorts.addOption(new Option('RESORT_21', '', ''));
    resorts.addOption(new Option('RESORT_22', '', ''));
    resorts.addOption(new Option('RESORT_23', '', ''));
    resorts.addOption(new Option('RESORT_24', '', ''));
    resorts.addOption(new Option('RESORT_25', '', ''));
    resorts.addOption(new Option('RESORT_26', '', ''));
    resorts.addOption(new Option('RESORT_27', '', ''));
    resorts.addOption(new Option('RESORT_28', '', ''));
    resorts.addOption(new Option('RESORT_29', '', ''));
    resorts.addOption(new Option('RESORT_30', '', ''));
    resorts.addOption(new Option('RESORT_31', '', ''));
    resorts.addOption(new Option('RESORT_32', '', ''));
    resorts.addOption(new Option('RESORT_33', '', ''));
    resorts.addOption(new Option('RESORT_34', '', ''));
    resorts.addOption(new Option('RESORT_35', '', ''));
    resorts.addOption(new Option('RESORT_36', '', ''));
    resorts.addOption(new Option('RESORT_37', '', ''));
    resorts.addOption(new Option('RESORT_38', '', ''));
    resorts.addOption(new Option('RESORT_39', '', ''));
    resorts.addOption(new Option('RESORT_40', '', ''));
    resorts.addOption(new Option('RESORT_41', '', ''));
    resorts.addOption(new Option('RESORT_42', '', ''));
    resorts.addOption(new Option('RESORT_43', '', ''));
    resorts.addOption(new Option('RESORT_44', '', ''));
    resorts.addOption(new Option('RESORT_45', '', ''));
    resorts.addOption(new Option('RESORT_46', '', ''));
    resorts.addOption(new Option('RESORT_47', '', ''));
    this.filters.addFilter(resorts);

    this.filters.addSort(new Option('SORT_DATE_ASC', 'Najwcześniejsze', '', true));
    this.filters.addSort(new Option('SORT_DATE_DESC', 'Najpóźniejsze'));
    this.filters.addSort(new Option('SORT_PRICE_ASC', 'Najtańsze'));
    this.filters.addSort(new Option('SORT_PRICE_DESC', 'Najdroższe'));

    this.parseURLQuery();
  }

  ngAfterViewInit(): void {
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({trigger: "hover"});
  }
}

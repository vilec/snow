import {Filter} from "./filter";
import {Option} from "./option";

export class Filters {
  filters: Filter[];
  sort : Option[];

  constructor() {
    this.filters = [];
    this.sort = [];
  }

  selectSort(sort: Option) {
    this.sort.forEach(s => s.active = s === sort);
  }

  addSort(option: Option) {
    this.sort.push(option);
  }

  addFilter(filter: Filter){
    this.filters.push(filter);
  }

  anySortSelected() : boolean {
    return this.sort.filter(s => s.active).length > 0;
  }

  selectDefault() {
    this.sort.forEach(sort => sort.active = (sort.code == "SORT_DATE_ASC"));
  }
}


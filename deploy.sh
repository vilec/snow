#!/usr/bin/env bash
SECONDS=0
echo "Building app.."
sbt dist
echo "Sending package to server.."
scp target/universal/snow-0.1.zip root@46.101.106.225:/opt/snowtribe/prod
echo "Redeploying app.."
ssh root@46.101.106.225 << EOF
cd /opt/snowtribe/prod
unzip snow-0.1.zip
rm snow-0.1.zip
rm snow-previous -r
monit unmonitor snow
pkill -f snow.pid
mv snow-current snow-previous 2>/dev/null
mv snow-0.1 snow-current 2>/dev/null
cd snow-current/bin
nohup ./snow  -Dhttp.port=80 -Dhttps.port=443 -Dplay.server.https.keyStore.path=/etc/cloudflare/live/keyStore.jks -Dplay.server.https.keyStore.password=fxavzAGfXCub4833P7Nx -Djdk.tls.ephemeralDHKeySize=2048 -Djdk.tls.rejectClientInitiatedRenegotiation=true -Dplay.evolutions.db.default.autoApply=true -Dlogger.resource=logback.prod.xml -Dconfig.resource=application.prod.conf -Dpidfile.path=/opt/snowtribe/prod/snow.pid > /dev/null 2>&1 &
monit monitor snow
exit
EOF
echo "Deployment done"
duration=$SECONDS
echo "It took $(($duration / 60)) minutes and $(($duration % 60)) seconds."

$(function () {

  $('#newsletter').on('change', function(e){
    if(e.target.checked){
      $('#collapseNewsletter').collapse('show');
    } else {
      $('#collapseNewsletter').collapse('hide');
    }
  });

  // Get the form.
  var form = $('#discount-form');

  // Get the messages div.
  var formMessages = $('#form-messages');

  // Set up an event listener for the contact form.
  $(form).submit(function (event) {
    // Stop the browser from submitting the form.
    event.preventDefault();

    // Serialize the form data.
    var formData = $(form).serialize();

    $('#discount-button').prop('disabled', true);
    var buttonText = $('#discount-button').html();
    $('#discount-button').html('Wysyłanie...');

    // Submit the form using AJAX.
    $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData
    }).done(function (response) {
      ga('send', { 'hitType': 'pageview', 'page': window.location.href + '/virtual/newsletter/', 'title': 'Subscribed to newsletter' });
      // Make sure that the formMessages div has the 'success' class.
      $('#error-message').hide();
      $('#success-message').show();

      $('#discount-button').remove();
      $('#close-button').remove();
      $('#discount-form').remove();

    }).fail(function (data) {
      $('#discount-button').prop('disabled', false);
      $('#error-message').show();
      $('#discount-button').html(buttonText);

    });

  });

  $('#discountModal').on('show.bs.modal', function (e) {
    ga('send', { 'hitType': 'pageview', 'page': window.location.href + '/virtual/discount/', 'title': 'Discount Modal Opened' });
    if (location.hostname != 'localhost') {
      gtag('event', 'conversion', {'send_to': 'AW-964614943/XrvRCL_qunYQn7b7ywM'});
    }
  })

});


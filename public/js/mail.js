$(function () {
    // Get the form.
    var form = $('#contact-form');

    // Get the messages div.
    var button = $('#contact-form button');

    // Set up an event listener for the contact form.
    $(form).submit(function (event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        // Serialize the form data.
        var formData = $(form).serialize();

      button.text("Wysyłanie...");
      button.attr("disabled", true);
      $('#error-message').hide();


      // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        }).done(function (response) {
            $('#success-message').show();
            form.hide();
        }).fail(function (data) {
            // Make sure that the formMessages div has the 'error' class.
          button.attr("disabled", false);
          $('#error-message').show();
          button.text("Wyślij wiadomość");
        });
    });

});

$(document).ready(function() {
  $('img[src$=".svg"]').each(function() {
    var $img = jQuery(this);
    var imgURL = $img.attr('src');
    var attributes = $img.prop("attributes");

    $.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');

      // Remove any invalid XML tags
      $svg = $svg.removeAttr('xmlns:a');

      // Loop through IMG attributes and apply on SVG
      $.each(attributes, function() {
        $svg.attr(this.name, this.value);
      });

      // Replace IMG with SVG
      $img.replaceWith($svg);
    }, 'xml');
  });

  $('#resortCarousel .carousel-control-next').click(function(e){
    e.preventDefault();
    $('#resortCarousel').carousel('next');

  });
  $('#resortCarousel .carousel-control-prev').click(function(e){
    e.preventDefault();
    $('#resortCarousel').carousel('prev');
  });

  $('#accommodationCarousel .carousel-control-next').click(function(e){
    e.preventDefault();
    $('#accommodationCarousel').carousel('next');

  });
  $('#accommodationCarousel .carousel-control-prev').click(function(e){
    e.preventDefault();
    $('#accommodationCarousel').carousel('prev');
  });
});

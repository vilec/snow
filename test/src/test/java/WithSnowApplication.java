package src.test.java;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.*;
import java.util.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import play.*;
import play.libs.*;
import play.mvc.*;
import play.test.*;
import static play.test.Helpers.*;

public class WithSnowApplication extends WithApplication {

  protected final Map<String, String[]> AUTH_HEADER = Maps.newHashMap(ImmutableMap.<String, String[]>builder()
  .put("Authorization", new String[] {"Bearer eyJhbGciOiJIUzI1NiJ9.eyIkaW50X3Blcm1zIjpbXSwic3ViIjoib3JnLnBhYzRqLmNvcmUucHJvZmlsZS5Db21tb25Qcm9maWxlI251bGwiLCIkaW50X3JvbGVzIjpbXSwiaWF0IjoxNDg5MzUxMDk3LCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJ1c2VybmFtZSI6InRlc3QifQ.w4ZELVko2yf3UHJ_rsvrRWIY86J1j3cnC07XLaU7faI"})
  .build());

  @Override
  public void startPlay() {
    super.startPlay();

  }

  protected void assertEvents(Result result, String... eventNames) {
    assertThat(result.status(), is(200));
    JsonNode json = Json.parse(contentAsString(result)).get("events");
    assertThat(json.size(), is(eventNames.length));
    for (int i = 0; i < eventNames.length; i++) {
      assertThat(json.get(i).get("name").asText(), is(eventNames[i]));
    }
  }

  protected Result callEvents(String url) {
    Http.RequestBuilder request = new Http.RequestBuilder()
      .headers(AUTH_HEADER)
      .method(GET)
      .uri(url);
    return route(app, request);
  }

}

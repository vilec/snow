package src.test.java.event;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.evolutions.Evolution;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import src.test.java.WithSnowApplication;

public class EventsPriceTest extends WithSnowApplication {

  Database database;

  @Before
  public void setupDatabase() {
    database = app.injector().instanceOf(Database.class);
    Evolutions.applyEvolutions(database);
    Evolutions.applyEvolutions(database, Evolutions.forDefault(
      new Evolution(
        999,
        "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event1', '2017-10-06', '2017-12-16', 1950, 1, 1, 100, 5, 'link1', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event2', '2017-11-06', '2017-11-16', 2149,  2, 1, 100, 5, 'link2', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event3', '2017-12-06', '2017-12-16', 1500,  1, 1, 100, 5, 'link3', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event4', '2017-12-08', '2017-12-16', 500, 2, 1, 100, 5, 'link4', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event5', '2018-01-01', '2018-01-16', 1200, 1, 1, 100, 5, 'link5', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event6', '2018-02-06', '2018-02-16', 5000, 2, 1, 100, 5, 'link6', 1);",
        "delete from event;"
      )
    ));
  }

  @After
  public void cleanDatabase() {
    Evolutions.cleanupEvolutions(database);
  }

  @Test
  public void allPricesEmptyQuery() {
    Result result = callEvents("/api/search?query=");
    assertEvents(result, "event1", "event2", "event3", "event4", "event5", "event6");
  }

  @Test
  public void singlePricesLess900Filter() {
    Result result = callEvents("/api/search?query=less_900");
    assertEvents(result, "event4");
  }

  @Test
  public void singlePrices9001200Filter() {
    Result result = callEvents("/api/search?query=between_900_1200");
    assertEvents(result, "event5");
  }

  @Test
  public void singlePrices12001500Filter() {
    Result result = callEvents("/api/search?query=between_1200_1500");
    assertEvents(result, "event3", "event5");
  }

  @Test
  public void singlePrices15002000Filter() {
    Result result = callEvents("/api/search?query=between_1500_2000");
    assertEvents(result, "event1", "event3");
  }

  @Test
  public void singlePrices20002500Filter() {
    Result result = callEvents("/api/search?query=between_2000_2500");
    assertEvents(result,  "event2");
  }

  @Test
  public void singlePricesMore2500Filter() {
    Result result = callEvents("/api/search?query=more_2500");
    assertEvents(result, "event6");
  }

  @Test
  public void multiplePricesFilter() {
    Result result = callEvents("/api/search?query=less_900,more_2500");
    assertEvents(result, "event4", "event6");
  }

  @Test
  public void multiplePricesFilterOverlap() {
    Result result = callEvents("/api/search?query=between_900_1200,between_1200_1500");
    assertEvents(result, "event3", "event5");
  }

  @Test
  public void allPricesFilter() {
    Result result = callEvents("/api/search?query=between_900_1200,less_900,between_1200_1500,between_1500_2000,between_2000_2500,more_2500");
    assertEvents(result, "event1", "event2", "event3", "event4", "event5", "event6");
  }


}

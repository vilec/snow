package src.test.java.event;

import api.event.EventRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.evolutions.Evolution;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import src.test.java.WithSnowApplication;

public class EventsPageTest extends WithSnowApplication {

  Database database;

  private int previousEventsPerPage;

  @Before
  public void setupDatabase() {
    previousEventsPerPage = EventRepository.EVENTS_PER_PAGE;
    EventRepository.EVENTS_PER_PAGE = 2;
    database = app.injector().instanceOf(Database.class);
    Evolutions.applyEvolutions(database);
    Evolutions.applyEvolutions(database, Evolutions.forDefault(
      new Evolution(
        999,
        "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event1', '2017-10-06', '2017-12-16', 1950, 1, 1, 100, 4, 'link1', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event2', '2017-11-06', '2017-11-16', 2149, 2, 1, 100, 4, 'link2', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event3', '2017-12-06', '2017-12-16', 1500, 1, 1, 100, 4, 'link3', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event4', '2017-12-08', '2017-12-16', 500, 2, 1, 100, 4, 'link4', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event5', '2018-01-01', '2018-01-16', 1200, 1, 1, 100, 4, 'link5', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event6', '2018-02-06', '2018-02-16', 5000, 2, 1, 100, 4, 'link6', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event7', '2018-02-07', '2018-02-16', 5000, 1, 1, 100, 4, 'link7', 1);",
        "delete from event;"
      )
    ));
  }

  @After
  public void cleanDatabase() {
    Evolutions.cleanupEvolutions(database);
    EventRepository.EVENTS_PER_PAGE = previousEventsPerPage;
  }

  @Test
  public void defaultPage() {
    Result result = callEvents("/api/search?query=");
    assertEvents(result, "event1", "event2");
  }

  @Test
  public void firstPage() {
    Result result = callEvents("/api/search?query=&page=1");
    assertEvents(result, "event1", "event2");
  }

  @Test
  public void secondPage() {
    Result result = callEvents("/api/search?query=&page=2");
    assertEvents(result, "event3", "event4");
  }

  @Test
  public void thirdPage() {
    Result result = callEvents("/api/search?query=&page=3");
    assertEvents(result, "event5", "event6");
  }

  @Test
  public void lastPage() {
    Result result = callEvents("/api/search?query=&page=4");
    assertEvents(result, "event7");
  }

  @Test
  public void emptyPage() {
    Result result = callEvents("/api/search?query=&page=5");
    assertEvents(result);
  }
}

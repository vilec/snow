package src.test.java.event;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.evolutions.Evolution;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import src.test.java.WithSnowApplication;

public class EventsDatesTest extends WithSnowApplication {

  Database database;

  @Before
  public void setupDatabase() {
    database = app.injector().instanceOf(Database.class);
    Evolutions.applyEvolutions(database);
    Evolutions.applyEvolutions(database, Evolutions.forDefault(
      new Evolution(
        999,
        "INSERT INTO `event` (`name`, `start`, `end`, `price`,  `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event1', '2017-10-06', '2017-10-16', 1950, 1, 1, 100, 5, 'link1', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event2', '2017-11-06', '2017-12-16', 2149, 2, 1, 100, 5, 'link2', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event3', '2017-12-31', '2018-01-05', 1500,  1, 1, 100, 5, 'link3', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event4', '2018-01-01', '2018-01-10', 500, 2, 1, 100, 5, 'link4', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event5', '2018-02-20', '2018-02-28', 1200,  1, 1, 100, 5, 'link5', 1);"
          + "INSERT INTO `event` (`name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('event6', '2018-02-06', '2018-02-16', 5000, 2, 1, 100, 5, 'link6', 1);",
        "delete from event;"
      )
    ));
  }

  @After
  public void cleanDatabase() {
    Evolutions.cleanupEvolutions(database);
  }

  @Test
  public void allEventsSortedByDate() {
    Result result = callEvents("/api/search?query=");
    assertEvents(result, "event1", "event2", "event3", "event4", "event6", "event5");
  }

  @Test
  public void singleDateWithinFilter() {
    Result result = callEvents("/api/search?query=october");
    assertEvents(result, "event1");
  }

  @Test
  public void singleDateStartsInFilter() {
    Result result = callEvents("/api/search?query=november");
    assertEvents(result, "event2");
  }

  @Test
  public void singleDateEndsInFilter() {
    Result result = callEvents("/api/search?query=december");
    assertEvents(result, "event2", "event3");
  }
  @Test
  public void singleDateInclusiveFromFilter() {
    Result result = callEvents("/api/search?query=january");
    assertEvents(result, "event3", "event4");
  }

  @Test
  public void singleDateInclusiveToFilter() {
    Result result = callEvents("/api/search?query=february");
    assertEvents(result, "event6", "event5");
  }

  @Test
  public void multipleDateInclusive() {
    Result result = callEvents("/api/search?query=january,new_year,december");
    assertEvents(result, "event2", "event3", "event4");
  }

  @Test
  public void allDates() {
    Result result = callEvents("/api/search?query=october,november,january,new_year,december,february");
    assertEvents(result, "event1","event2", "event3", "event4", "event6", "event5");
  }
}

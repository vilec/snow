package src.test.java.event;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.evolutions.Evolution;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import src.test.java.WithSnowApplication;

public class EventsTransportTest extends WithSnowApplication {

  Database database;

  @Before
  public void setupDatabase() {
    database = app.injector().instanceOf(Database.class);
    Evolutions.applyEvolutions(database);
    Evolutions.applyEvolutions(database, Evolutions.forDefault(
      new Evolution(
        999,
        "INSERT INTO `event` (`id`, `name`, `start`, `end`, `price`,  `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('1', 'event1', '2017-10-06', '2017-12-16', 1950, 1, 1, 100, 5, 'link1', 1);"
          + "INSERT INTO `event` (`id`, `name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('2', 'event2', '2017-11-06', '2017-11-16', 2149, 2, 1, 100, 5, 'link2', 1);"
          + "INSERT INTO `event` (`id`, `name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('3', 'event3', '2017-12-06', '2017-12-16', 1500, 1, 1, 100, 5, 'link3', 1);"
          + "INSERT INTO `event` (`id`, `name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('4', 'event4', '2017-12-08', '2017-12-16', 500, 2, 1, 100, 5, 'link4', 1);"
          + "INSERT INTO `event` (`id`, `name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('5', 'event5', '2018-01-01', '2018-01-16', 1200, 1, 1, 100, 5, 'link5', 1);"
          + "INSERT INTO `event` (`id`, `name`, `start`, `end`, `price`, `resort_id`, `company_id`, people, stars, link, category_id) VALUES ('6', 'event6', '2018-02-06', '2018-02-16', 5000, 2, 1, 100, 5, 'link6', 1);"
          + "INSERT INTO `event_transport` (`event_id`, `transport_id`) VALUES ('1', '3');"
          + "INSERT INTO `event_transport` (`event_id`, `transport_id`) VALUES ('2', '2');"
          + "INSERT INTO `event_transport` (`event_id`, `transport_id`) VALUES ('3', '3');"
          + "INSERT INTO `event_transport` (`event_id`, `transport_id`) VALUES ('4', '1');"
          + "INSERT INTO `event_transport` (`event_id`, `transport_id`) VALUES ('5', '2');"
          + "INSERT INTO `event_transport` (`event_id`, `transport_id`) VALUES ('6', '1');",
        "delete from event_transport; delete from event;"
      )
    ));
  }

  @After
  public void cleanDatabase() {
    Evolutions.cleanupEvolutions(database);
  }

  @Test
  public void allTransportsSortedByDate() {
    Result result = callEvents("/api/search?query=");
    assertEvents(result, "event1", "event2", "event3", "event4", "event5", "event6");
  }

  @Test
  public void singleTransportFilter() {
    Result result = callEvents("/api/search?query=car");
    assertEvents(result, "event2", "event5");
  }

  @Test
  public void multipleTransportsFilter() {
    Result result = callEvents("/api/search?query=car,plane");
    assertEvents(result, "event1", "event2", "event3", "event5");
  }

  @Test
  public void allTransportsFilter() {
    Result result = callEvents("/api/search?query=car,plane,bus");
    assertEvents(result, "event1", "event2", "event3", "event4", "event5", "event6");
  }

}

# --- !Ups
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accommodation
# ------------------------------------------------------------

CREATE TABLE `accommodation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accommodationType` varchar(255) NOT NULL DEFAULT '',
  `order` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table company
# ------------------------------------------------------------

CREATE TABLE `company` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `partner` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `discount_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table discount_event
# ------------------------------------------------------------

CREATE TABLE `discount_event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event
# ------------------------------------------------------------

CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL DEFAULT '',
  `start` date NOT NULL,
  `end` date NOT NULL,
  `price` int(11) NOT NULL,
  `people` int(11) NOT NULL,
  `stars` int(10) NOT NULL,
  `rooms` varchar(20) NOT NULL DEFAULT '',
  `resort_id` int(11) unsigned NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `resort_fk` (`resort_id`),
  CONSTRAINT `event_company_fk` FOREIGN KEY (`id`) REFERENCES `company` (`id`),
  CONSTRAINT `resort_fk` FOREIGN KEY (`resort_id`) REFERENCES `resort` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event_accommodation
# ------------------------------------------------------------

CREATE TABLE `event_accommodation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accommodation_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `offered` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_accommodation_event_fk` (`event_id`),
  KEY `event_accommodation_accommodation_fk` (`accommodation_id`),
  CONSTRAINT `event_accommodation_accommodation_fk` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation` (`id`),
  CONSTRAINT `event_accommodation_event_fk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event_feature
# ------------------------------------------------------------

CREATE TABLE `event_feature` (
  `feature_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `event_feature_event_fk` (`event_id`),
  KEY `event_feature_feature_fk` (`feature_id`),
  CONSTRAINT `event_feature_event_fk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `event_feature_feature_fk` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event_image
# ------------------------------------------------------------

CREATE TABLE `event_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `event_image_even_fk` (`event_id`),
  CONSTRAINT `event_image_even_fk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event_price
# ------------------------------------------------------------

CREATE TABLE `event_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `kind` varchar(255) NOT NULL DEFAULT '',
  `value` int(11) unsigned DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_price_event_fk` (`event_id`),
  KEY `event_price_price_fk` (`price_id`),
  CONSTRAINT `event_price_event_fk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `event_price_price_fk` FOREIGN KEY (`price_id`) REFERENCES `price` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event_transport
# ------------------------------------------------------------

CREATE TABLE `event_transport` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transport_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_transport_event_fk` (`event_id`),
  KEY `event_transport_transport_fk` (`transport_id`),
  CONSTRAINT `event_transport_event_fk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `event_transport_transport_fk` FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table feature
# ------------------------------------------------------------

CREATE TABLE `feature` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featureType` varchar(255) NOT NULL DEFAULT '',
  `order` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table price
# ------------------------------------------------------------

CREATE TABLE `price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `priceType` varchar(255) NOT NULL DEFAULT '',
  `order` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table resort
# ------------------------------------------------------------

CREATE TABLE `resort` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `country` varchar(128) NOT NULL DEFAULT '',
  `slope_length_easy` int(11) DEFAULT '0',
  `slope_length_medium` int(11) DEFAULT '0',
  `slope_length_hard` int(11) DEFAULT '0',
  `slope_length_expert` int(11) DEFAULT '0',
  `altitude` int(11) DEFAULT '0',
  `map_image` varchar(255) DEFAULT NULL,
  `snowpark` tinyint(1) DEFAULT '0',
  `freeride` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transport
# ------------------------------------------------------------

CREATE TABLE `transport` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transportType` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(11) DEFAULT NULL,
  `admin` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  CONSTRAINT `user_company_fk` FOREIGN KEY (`id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# --- !Downs
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE accommodation;
DROP TABLE discount_event;
DROP TABLE event;
DROP TABLE company;
DROP TABLE event_accommodation;
DROP TABLE event_feature;
DROP TABLE event_image;
DROP TABLE event_price;
DROP TABLE event_transport;
DROP TABLE feature;
DROP TABLE price;
DROP TABLE resort;
DROP TABLE transport;
DROP TABLE user;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# --- !Ups
ALTER TABLE `event` ADD `url` INT  NULL  DEFAULT NULL  AFTER `company_id`;
ALTER TABLE `event` CHANGE `url` `url` VARCHAR(255)  NULL  DEFAULT '';
ALTER TABLE `event` MODIFY COLUMN `url` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci AFTER `rooms`;

# --- !Downs
ALTER TABLE `event` DROP `url`;

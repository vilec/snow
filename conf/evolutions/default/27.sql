# --- !Ups
CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `event` ADD `category_id` INT(11)  UNSIGNED  NULL  DEFAULT NULL  AFTER `company_id`;
ALTER TABLE `event` ADD CONSTRAINT `event_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

INSERT INTO `category` (`id`, `code`, `name`, `icon`)
VALUES
  (1, 'ADULT', 'Dorośli', 'confetti.svg'),
  (2, 'FREERIDE', 'Freeride', 'snowboarder.svg'),
  (3, 'CHILDREN', 'Dzieci', 'spa.svg'),
  (4, 'FAMILY', 'Rodzinne', 'poker.svg'),
  (5, 'TRAINING', 'Szkolenia', 'ski.svg');

UPDATE event SET category_id = 1;
ALTER TABLE `event` CHANGE `category_id` `category_id` INT(11)  UNSIGNED  NOT NULL;

# --- !Downs
ALTER TABLE `event` DROP FOREIGN KEY `event_category_id`;
ALTER TABLE `event` DROP `category_id`;
DROP TABLE `category`;


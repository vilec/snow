# --- !Ups
ALTER TABLE `event` ADD `link` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `company_id`;
ALTER TABLE `event` MODIFY COLUMN `link` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL AFTER `name`;



# --- !Downs
ALTER TABLE `event` DROP `link`;

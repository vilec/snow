# --- !Ups
ALTER TABLE `event_transport` ADD `kind` VARCHAR(20)  NULL  DEFAULT NULL  AFTER `event_id`;
ALTER TABLE `event_transport` CHANGE `kind` `kind` VARCHAR(20)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT '';
ALTER TABLE `event_transport` CHANGE `kind` `kind` VARCHAR(20)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;
ALTER TABLE `event_transport` ADD `value` INT(11)  NULL  DEFAULT '0'  AFTER `kind`;

# --- !Downs
ALTER TABLE `event_transport` DROP `kind`;
ALTER TABLE `event_transport` DROP `value`;

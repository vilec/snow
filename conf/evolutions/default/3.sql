# --- !Ups
ALTER TABLE `feature` CHANGE `featureType` `code` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

# --- !Downs
ALTER TABLE `feature` CHANGE `code` `featureType` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

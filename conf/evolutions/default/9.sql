# --- !Ups
delete from resort;

ALTER TABLE `resort` CHANGE `slope_length_easy` `slope_length_easy` VARCHAR(11)  NULL  DEFAULT '0';
ALTER TABLE `resort` CHANGE `slope_length_medium` `slope_length_medium` VARCHAR(11)  NULL  DEFAULT '0';
ALTER TABLE `resort` CHANGE `slope_length_hard` `slope_length_hard` VARCHAR(11)  NULL  DEFAULT '0';
ALTER TABLE `resort` CHANGE `slope_length_expert` `slope_length_expert` VARCHAR(11)  NULL  DEFAULT '0';

ALTER TABLE `resort` ADD `length` INT  NULL  DEFAULT NULL  AFTER `freeride`;
ALTER TABLE `resort` CHANGE `length` `length` INT(11)  NULL  DEFAULT '0';
ALTER TABLE `resort` MODIFY COLUMN `length` INT(11) DEFAULT '0' AFTER `slope_length_expert`;


INSERT INTO `resort` (`id`, `name`, `country`, `slope_length_easy`, `slope_length_medium`, `slope_length_hard`, `slope_length_expert`, `length`, `altitude`, `snowpark`, `freeride`, `map_image`)
VALUES
(1, 'Les 2 Alpes', 'FRANCE', '17', '45', '22', '12', '227', '3600', 1, 1, 'http://www.les2alpes.com/images/contenu_site/glisse/2017-Plan_hiver_L2A.pdf'),
(2, '3 Doliny', 'FRANCE', '54', '140', '113', '35', '600', '3230', 1, 1, 'http://www.stations2ski.fr/images/media/domaines/les-3-vallees/plan-pistes.jpg'),
(3, 'Val Thorens (3 Doliny)', 'FRANCE', '11', '29', '30', '9', '150', '3200', 1, 1, 'http://www.valthorens.com/pdf/plans/ski/Plan_3_Vallees_2016-17_BD.pdf'),
(4, "Alpe d'Huez", 'FRANCE', '50', '22', '25', '14', '250', '3330', 1, 1, null),
(5, 'Val di Sole (Skirama)', 'ITALY', '0km', '45km', '56km', '35km', '136', '3016', 1, 1, 'http://www.valdisole.net/upload/cms/skimap_2015_2016.jpg'),
(6, 'Les Menuires (3 Doliny)', 'FRANCE', '12', '44', '22', '8', '160', '2850', 1, 1, 'http://media.lesmenuires.com/2017/03/plan-pistes-lesmenuires-saintmartin2016-2017.pdf'),
(7, 'Val di Fiemme', 'ITALY', '0', '36', '37', '13', '100', '2498', 1, 1, 'https://www.visitfiemme.it/media/active-relax/neve/ski-map-val-di-fiemme-obereggen.jpg'),
(8, 'La Rosiere', 'FRANCE', '8', '25', '32', '15', '160', '2650', 1, 1, 'http://www.larosiere.net/wp-content/uploads/2016/06/LA-ROSIERE_VUE-GENERAL_V3-2016-2017-BD.pdf'),
(9, 'Risoul-Vars', 'FRANCE', '18', '49', '38', '10', '185', '2750', 1, 1, 'https://www.risoul.com/medias/images/info_pages/plan-des-pistes-foret-blanche-1799.jpg'),
(10, 'Livigno', 'ITALY', '0km', '45km', '50km', '15km', '110', '2798', 1, 1, 'http://www.skipasslivigno.com/wp-content/uploads/mappa_2017.png?x72237'),
(11, 'Pitztal', 'AUSTRIA', '0', '11', '26', '10', '80', '3340', 1, 1, 'https://cdn.snowplaza.nl/content/WinterPanos/2500/12152.jpg'),
(12, 'Chamrousse', 'FRANCE', '8', '14', '13', '6', '90', '2250', 1, 1, 'https://www.chamrousse.com/medias/plan/chamrousse-fond-plan-webcam[HIVER_FR[.jpg'),
(13, 'Stubai', 'AUSTRIA', '0', '13', '7', '3', '64', '3210', 1, 1, 'http://www.skiresort.info/typo3temp/_processed_/fe/e1/11/1a/621d98f78d.jpg'),
(14, 'Laax', 'SWITZERLAND', '0km', '77km', '90km', '68km', '235', '3018', 1, 1, 'https://www.laax.com/fileadmin/Daten/Dokumente/PDF/Pistenplan_2016_17_EN_low.pdf'),
(15, 'Les Orres', 'FRANCE',	'7', '6', '20', '4', '100', '2720', 1, 1, 'https://www.lesorres.com/sites/lesorres.com/files/plan-des-pistes.pdf');




# --- !Downs
delete from resort;
ALTER TABLE `resort` DROP `length`;

INSERT INTO `resort` (`id`, `name`, `country`, `slope_length_easy`, `slope_length_medium`, `slope_length_hard`, `slope_length_expert`, `altitude`, `map_image`, `snowpark`, `freeride`)
VALUES
	(1, 'Hintertoux', 'AUSTRIA', 0, 0, 0, 0, 0, NULL, 0, 0),
	(2, 'Szczyrk', 'POLAND', 0, 0, 0, 0, 0, NULL, 0, 0);



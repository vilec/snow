# --- !Ups
ALTER TABLE `event` ADD `price_from` TINYINT  NULL  DEFAULT 0  AFTER `company_id`;
ALTER TABLE `event` MODIFY COLUMN `price_from` TINYINT(1) DEFAULT '0' AFTER `price`;
ALTER TABLE `event` ADD `price_notes` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `company_id`;
ALTER TABLE `event` MODIFY COLUMN `price_notes` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL AFTER `price_from`;

# --- !Downs
ALTER TABLE `event` DROP `price_notes`;
ALTER TABLE `event` DROP `price_from`;

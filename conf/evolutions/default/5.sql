# --- !Ups
ALTER TABLE `transport` CHANGE `transportType` `code` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';
ALTER TABLE `transport` ADD `icon` VARCHAR(255)  NULL  DEFAULT ''  AFTER `name`;
UPDATE `transport` SET `icon` = 'transportation_bus-front-12' WHERE `code` = 'BUS';
UPDATE `transport` SET `icon` = 'shopping_delivery-fast' WHERE `code` = 'CAR';
UPDATE `transport` SET `icon` = 'ui-1_send' WHERE `code` = 'PLANE';

# --- !Downs
ALTER TABLE `transport` DROP `icon`;
ALTER TABLE `transport` CHANGE `code` `transportType` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

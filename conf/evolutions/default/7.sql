# --- !Ups
ALTER TABLE `price` CHANGE `priceType` `code` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

# --- !Downs
ALTER TABLE `price` CHANGE `code` `priceType` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

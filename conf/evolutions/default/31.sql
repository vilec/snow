# --- !Ups
ALTER TABLE `event` ADD `notes` VARCHAR(512)  NULL  DEFAULT NULL  AFTER `category_id`;

# --- !Downs
ALTER TABLE `event` DROP `notes`;

# --- !Ups
update resort set map_image = 'les2a-map.pdf' where name = 'Les 2 Alpes';
update resort set map_image = 'les3val-map.jpg' where name = '3 Doliny';
update resort set map_image = 'valtho-map.pdf' where name = 'Val Thorens (3 Doliny)';
update resort set map_image = '' where name = "Alpe d'Huez";
update resort set map_image = 'valdisol-map.jpg' where name = 'Val di Sole (Skirama)';
update resort set map_image = 'lesmenu-map.pdf' where name = 'Les Menuires (3 Doliny)';
update resort set map_image = 'valdifie-map.jpg' where name = 'Val di Fiemme';
update resort set map_image = 'larosiere-map.pdf' where name = 'La Rosiere';
update resort set map_image = 'risoul-vars-map.jpg' where name = 'Risoul-Vars';
update resort set map_image = 'livigno-map.png' where name = 'Livigno';
update resort set map_image = 'pitztal-map.jpg' where name = 'Pitztal';
update resort set map_image = 'cham-map.jpg' where name = 'Chamrousse';
update resort set map_image = 'stubai-map.jpg' where name = 'Stubai';
update resort set map_image = 'laax-map.pdf' where name = 'Laax';
update resort set map_image = 'lesore-map.pdf' where name = 'Les Orres';

# --- !Downs

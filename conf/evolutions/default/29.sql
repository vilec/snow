# --- !Ups
CREATE TABLE `check_result` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '',
  `date` datetime NOT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `check_result_event_fk` (`event_id`),
  CONSTRAINT `check_result_event_fk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# --- !Downs
drop table check_result;

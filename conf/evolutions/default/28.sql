# --- !Ups
update category set name = 'Studenci i dorośli', icon = 'chalet.svg' where id = 1;
update category set name = 'Rodzinne', icon = 'family.svg' where id = 4;
update category set name = 'Dla dzieci', icon = 'children.svg' where id = 3;
update category set name = 'Freeride', icon = 'mountain.svg' where id = 2;
update category set name = 'Szkolenia', icon = 'snowboarder.svg' where id = 5;

# --- !Downs

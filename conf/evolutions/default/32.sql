# --- !Ups
ALTER TABLE `resort` ADD `description` TEXT  NULL  AFTER `freeride`;
ALTER TABLE `resort` MODIFY COLUMN `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci AFTER `country`;
ALTER TABLE `resort` ADD `link` VARCHAR(128)  NULL  DEFAULT NULL  AFTER `freeride`;
ALTER TABLE `resort` MODIFY COLUMN `link` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL AFTER `name`;

UPDATE `resort` SET `link` = 'les-2-alpes' WHERE `id` = '1';
UPDATE `resort` SET `link` = '3-doliny' WHERE `id` = '2';
UPDATE `resort` SET `link` = 'val-thorens' WHERE `id` = '3';
UPDATE `resort` SET `link` = 'val-thorens-3-doliny' WHERE `id` = '3';
UPDATE `resort` SET `link` = 'alpe-dhuez' WHERE `id` = '4';
UPDATE `resort` SET `link` = 'val-di-sole' WHERE `id` = '5';
UPDATE `resort` SET `link` = 'les-menuires' WHERE `id` = '6';
UPDATE `resort` SET `link` = 'val-di-fiemme' WHERE `id` = '7';
UPDATE `resort` SET `link` = 'francja-les-2-alpes' WHERE `id` = '1';
UPDATE `resort` SET `link` = 'les-2-alpes' WHERE `id` = '1';
UPDATE `resort` SET `link` = 'la-rosiere' WHERE `id` = '8';
UPDATE `resort` SET `link` = 'risoul-vars' WHERE `id` = '9';
UPDATE `resort` SET `link` = 'livigno' WHERE `id` = '10';
UPDATE `resort` SET `link` = 'pitztal' WHERE `id` = '11';
UPDATE `resort` SET `link` = 'chamrousse' WHERE `id` = '12';
UPDATE `resort` SET `link` = 'stubai' WHERE `id` = '13';
UPDATE `resort` SET `link` = 'laax' WHERE `id` = '14';
UPDATE `resort` SET `link` = 'les-orres' WHERE `id` = '15';
UPDATE `resort` SET `link` = 'tignes' WHERE `id` = '16';
UPDATE `resort` SET `link` = 'gudauri' WHERE `id` = '17';
UPDATE `resort` SET `link` = 'bialy-dunajec' WHERE `id` = '18';
UPDATE `resort` SET `link` = 'mont-blanc-chamonix' WHERE `id` = '19';
UPDATE `resort` SET `link` = 'verbier' WHERE `id` = '20';
UPDATE `resort` SET `link` = 'vaysonnaz' WHERE `id` = '21';
UPDATE `resort` SET `link` = 'montecampione' WHERE `id` = '23';
UPDATE `resort` SET `link` = 'karkonosze' WHERE `id` = '24';
UPDATE `resort` SET `link` = 'serre-chevalier' WHERE `id` = '25';
UPDATE `resort` SET `link` = 'les-arcs' WHERE `id` = '26';
UPDATE `resort` SET `link` = 'orelle-3-doliny' WHERE `id` = '28';
UPDATE `resort` SET `link` = 'les-sybelles' WHERE `id` = '29';
UPDATE `resort` SET `link` = 'valmeinier' WHERE `id` = '30';
UPDATE `resort` SET `link` = 'svanetia' WHERE `id` = '31';
UPDATE `resort` SET `link` = 'czarny-gron' WHERE `id` = '32';
UPDATE `resort` SET `link` = 'wierchomla' WHERE `id` = '33';
UPDATE `resort` SET `link` = 'folgaria-lavarone' WHERE `id` = '34';
UPDATE `resort` SET `link` = 'ski-safari-nassfel-bad-kleinkirchheim-gerlitzen-alpe-molltaler-gloetscher' WHERE `id` = '35';
UPDATE `resort` SET `link` = 'jasna-chopok' WHERE `id` = '36';
UPDATE `resort` SET `link` = 'isola2000' WHERE `id` = '37';
UPDATE `resort` SET `link` = 'pra-loup' WHERE `id` = '38';
UPDATE `resort` SET `link` = 'ski-safari-kranjska-gora-vogel-ski-center-krvavec-kanin-sella-nevea-cerkno' WHERE `id` = '39';
UPDATE `resort` SET `link` = 'cortina-dampezzo' WHERE `id` = '40';
UPDATE `resort` SET `link` = 'sainte-foy-tarentaise' WHERE `id` = '41';
UPDATE `resort` SET `link` = 'solden' WHERE `id` = '42';
UPDATE `resort` SET `link` = 'murzasichle' WHERE `id` = '43';
UPDATE `resort` SET `link` = 'puy-sant-vincent' WHERE `id` = '44';
UPDATE `resort` SET `link` = 'vaujany' WHERE `id` = '45';
UPDATE `resort` SET `link` = 'la-plagne' WHERE `id` = '47';


# --- !Downs
ALTER TABLE `resort` DROP `link`;
ALTER TABLE `resort` DROP `description`;

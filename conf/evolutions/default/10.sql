# --- !Ups
CREATE TABLE `resort_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resort_id` int(11) unsigned NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `resort_image_resort_fk` (`resort_id`),
  CONSTRAINT `resort_image_resort_fk` FOREIGN KEY (`resort_id`) REFERENCES `resort` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# --- !Downs
drop table resort_image;

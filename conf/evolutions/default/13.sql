# --- !Ups
delete from feature;
INSERT INTO `feature` (`id`, `code`, `order`, `icon`, `name`)
VALUES
  (1, 'BOARD_GAMES', 1, 'business_bulb-63', 'Gry planszowe'),
  (2, 'WORKSHOPS', 2, 'business_bulb-63', 'Warsztaty'),
  (3, 'ICERINK', 3, 'business_bulb-63', 'Lodowisko'),
  (4, 'SPA', 4, 'business_bulb-63', 'Spa'),
  (5, 'PARTY', 5, 'business_bulb-63', 'Imprezy'),
  (6, 'TOURNAMENTS', 6, 'business_bulb-63', 'Konkursy'),
  (7, 'MUSIC', 7, 'business_bulb-63', 'DJ / Muzyka na żywo'),
  (8, 'EQUIPMENT_TESTS', 8, 'business_bulb-63', 'Testy sprzętu'),
  (9, 'PARAGLIDER', 9, 'business_bulb-63', 'Lot na paralotni'),
  (10, 'APRES_SKI', 10, 'business_bulb-63', 'Apres SKI');


# --- !Downs

# --- !Ups
ALTER TABLE `company` ADD `price_xpath` VARCHAR(512)  NULL  DEFAULT NULL  AFTER `discount_code`;

# --- !Downs
alter table company drop column price_xpath;

# --- !Ups
ALTER TABLE `event` ADD `active` TINYINT(1)  NOT NULL  DEFAULT '1'  AFTER `company_id`;
ALTER TABLE `event` MODIFY COLUMN `active` TINYINT(1) NOT NULL DEFAULT '1' AFTER `name`;
ALTER TABLE `company` ADD `active` TINYINT(1)  NOT NULL  DEFAULT '1'  AFTER `discount_code`;
ALTER TABLE `company` MODIFY COLUMN `active` TINYINT(1) NOT NULL DEFAULT '1' AFTER `name`;

# --- !Downs
ALTER TABLE `event` DROP `active`;
ALTER TABLE `company` DROP `active`;

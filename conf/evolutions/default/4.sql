# --- !Ups
ALTER TABLE `accommodation` CHANGE `accommodationType` `code` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

# --- !Downs
ALTER TABLE `accommodation` CHANGE `code` `accommodationType` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';

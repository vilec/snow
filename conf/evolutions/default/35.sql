# --- !Ups
CREATE TABLE `country` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `link` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `name` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `variant` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `flag` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO `country` (`id`, `code`, `link`, `name`, `variant`, `description`, `flag`)
VALUES
  (1, 'FRANCE', 'francja', 'Francja', 'we Francji', 'Desc', '195-france.svg'),
  (2, 'ITALY', 'wlochy', 'Włochy', 'we Włoszech', 'Desc', '013-italy.svg'),
  (3, 'AUSTRIA', 'austria', 'Austria', 'w Austrii', 'Desc', '003-austria.svg'),
  (4, 'SWITZERLAND', 'szwajcaria', 'Szwajcaria', 'w Szwajcarii', 'Desc', '205-switzerland.svg'),
  (5, 'CZECH', 'czechy', 'Czechy', 'w Czechach', 'Desc', '149-czech-republic.svg'),
  (6, 'SLOVAKIA', 'slowacja', 'Słowacja', 'na Słowacji', 'Desc', '091-slovakia.svg'),
  (7, 'SLOVENIA', 'slowenia', 'Słowenia', 'w Słoweni', 'Desc', '010-slovenia.svg'),
  (8, 'GEORGIA', 'gruzja', 'Gruzja', 'w Gruzji', 'Desc', '256-georgia.svg'),
  (9, 'POLAND', 'polska', 'Polska', 'w Polsce', 'Desc', '211-poland.svg');


ALTER TABLE `resort` ADD `country_id` INT  UNSIGNED  NULL  AFTER `freeride`;
ALTER TABLE `resort` MODIFY COLUMN `country_id` INT(10) UNSIGNED DEFAULT NULL AFTER `country`;

update resort  r set country_id = (select id from country c where r.country = c.code);
ALTER TABLE `resort` CHANGE `country_id` `country_id` INT(10)  UNSIGNED  NOT NULL;

# --- !Downs
ALTER TABLE `resort` DROP `country_id`;
DROP TABLE `country`;

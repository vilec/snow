# --- !Ups
ALTER TABLE `event` ADD UNIQUE INDEX (`link`);
ALTER TABLE `event` CHANGE `link` `link` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';



# --- !Downs
ALTER TABLE `event` DROP INDEX `link`;
ALTER TABLE `event` CHANGE `link` `link` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT '';

# --- !Ups
INSERT INTO `company` (`id`, `name`, `logo`, `partner`, `url`, `discount`, `discount_code`)
VALUES
	(1, 'SnowTribe.pl', 'snowtribe-logo.png', 0, NULL, 0, NULL),
	(2, 'Feel The Flow', 'snow1.png', 0, 'http://feeltheflow.pl/', 0, NULL),
	(3, 'Snow Motion', 'snow2.png', 1, 'http://snowmotion.pl/', 50, 'SNWTRB-MOTION'),
	(4, 'SnowShow', 'snow3.png', 1, 'http://snowshow.pl', 30, 'SNWTRB-SSHOW'),
	(5, 'SnowEvents', 'snow-aplz.png', 0, 'http://snowevents.pl/', 0, NULL),
	(6, 'EH School', 'snow1.png', 0, 'http://www.ehschool.pl/', 50, 'SNWTRB-EHSCHOOL');

INSERT INTO `user` (`id`, `username`, `password`, `email`, `company_id`, `admin`)
VALUES
	(1, 'vilec', '$2a$10$V6Va5oTwK9ugIajLMWcPJ.1wIZpz/CcW5GsWBqbqy7pYrMKt46GRm', 'vilec@test.com', 1, 1),
	(2, 'snowshow', '$2a$10$V6Va5oTwK9ugIajLMWcPJ.1wIZpz/CcW5GsWBqbqy7pYrMKt46GRm', 'snow@test.com', 4, 0),
	(3, 'feeltheflow', '$2a$10$V6Va5oTwK9ugIajLMWcPJ.1wIZpz/CcW5GsWBqbqy7pYrMKt46GRm', 'feel@test.com', 2, 0),
	(4, 'snowmotion', '$2a$10$V6Va5oTwK9ugIajLMWcPJ.1wIZpz/CcW5GsWBqbqy7pYrMKt46GRm', 'test@test.com', 3, 0);

INSERT INTO `accommodation` (`id`, `accommodationType`, `order`, `name`)
VALUES
	(1, 'WIFI', 1, 'WiFi'),
	(2, 'BATHROOM', 2, 'Łazienka w pokoju'),
	(3, 'KITCHEN', 3, 'Aneks kuchenny'),
	(4, 'TV', 4, 'TV'),
	(5, 'BALCONY', 5, 'Balkon'),
	(6, 'POOL', 6, 'Basen / SPA'),
	(7, 'PARKING', 7, 'Parking');

INSERT INTO `feature` (`id`, `featureType`, `order`, `icon`, `name`)
VALUES
	(1, 'PARTY', 1, 'business_bulb-63', 'Imprezy'),
	(2, 'TOURNAMENTS', 2, 'business_badge', 'Konkursy');

INSERT INTO `price` (`id`, `priceType`, `order`, `name`)
VALUES
	(1, 'SKI_PASS', 1, 'Ski pass'),
	(2, 'FOOD', 2, 'Wyżywienie'),
	(3, 'TRANSPORT', 3, 'Transport'),
	(4, 'INSURANCE', 4, 'Ubezpieczenie'),
	(5, 'TAX', 5, 'Taksa klimatyczna'),
	(6, 'TRAINING', 6, 'Szkolenie'),
	(7, 'CLEANING', 7, 'Sprzątanie'),
	(8, 'BEDDING', 8, 'Pościel'),
	(9, 'SKIPASS_DEPOSIT', 9, 'Kaucja zwrotna za skipass'),
	(10, 'ACCOMMODATION_DEPOSIT', 10, 'Kaucja zwrotna za nocleg');

INSERT INTO `resort` (`id`, `name`, `country`, `slope_length_easy`, `slope_length_medium`, `slope_length_hard`, `slope_length_expert`, `altitude`, `map_image`, `snowpark`, `freeride`)
VALUES
	(1, 'Hintertoux', 'AUSTRIA', 0, 0, 0, 0, 0, NULL, 0, 0),
	(2, 'Szczyrk', 'POLAND', 0, 0, 0, 0, 0, NULL, 0, 0);

INSERT INTO `transport` (`id`, `transportType`, `name`)
VALUES
	(1, 'BUS', 'Autbus'),
	(2, 'CAR', 'Dojazd własny'),
	(3, 'PLANE', 'Samolot');


# --- !Downs
delete from `user`;
delete from `company`;
delete from `accommodation`;
delete from `feature`;
delete from `price`;
delete from `resort`;
delete from `transport`;


# --- !Ups
CREATE TABLE `feature_resort` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `order` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `feature_resort` (`id`, `code`, `order`, `icon`, `name`)
VALUES
  (1, 'OFF_PISTE', 0, NULL, 'Jazda poza trasami'),
  (2, 'SKITOURING', 1, NULL, 'Skitoury'),
  (3, 'HELI_SKIING', 2, NULL, 'Heli-ski'),
  (4, 'HALFPIPE', 3, NULL, 'Half-pipe'),
  (5, 'BORDERCROSS', 4, NULL, 'Bordercross'),
  (6, 'SNOWPARK', 5, NULL, 'Snowpark'),
  (7, 'KIDS_SLOPE', 6, NULL, 'Stok dla dzieci'),
  (8, 'LIGHTS', 7, NULL, 'Stok oświetlony');

CREATE TABLE `resort_feature` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resort_id` int(11) unsigned NOT NULL,
  `feature_resort_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resort_feature_fk` (`resort_id`),
  KEY `resort_Feature_feature_resort_fk` (`feature_resort_id`),
  CONSTRAINT `resort_Feature_feature_resort_fk` FOREIGN KEY (`feature_resort_id`) REFERENCES `feature_resort` (`id`),
  CONSTRAINT `resort_feature_fk` FOREIGN KEY (`resort_id`) REFERENCES `resort` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

# --- !Downs
Drop table feature_resort;
Drop table resort_feature;

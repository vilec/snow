# --- !Ups
ALTER TABLE `event` CHANGE `people` `people` INT(11)  NULL  DEFAULT NULL;

# --- !Downs
ALTER TABLE `event` CHANGE `people` `people` INT(11)  NOT NULL;

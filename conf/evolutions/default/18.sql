# --- !Ups
ALTER TABLE `event_transport` ADD `currency` VARCHAR(10)  NULL  DEFAULT NULL  AFTER `value`;

# --- !Downs
ALTER TABLE `event_transport` DROP `currency`;

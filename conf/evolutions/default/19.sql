# --- !Ups
update feature set name = 'Szkolenie narciarskie' where id = 11;

INSERT INTO `feature` (`id`, `code`, `order`, `icon`, `name`)
VALUES
  (12, 'GOPRO', 12, 'business_bulb-63', 'Wypożyczenie GoPro'),
  (13, 'POKER', 13, 'business_bulb-63', 'Turniej Pokera');


update feature set icon = 'horse.svg' where id = 1;
update feature set icon = 'light-bulb.svg' where id = 2;
update feature set icon = 'ice-skating.svg' where id = 3;
update feature set icon = 'spa.svg' where id = 4;
update feature set icon = 'confetti.svg' where id = 5;
update feature set icon = 'cup.svg' where id = 6;
update feature set icon = 'musical-note.svg' where id = 7;
update feature set icon = 'ski.svg' where id = 8;
update feature set icon = 'parachute.svg' where id = 9;
update feature set icon = 'beer.svg' where id = 10;
update feature set icon = 'snowboarder.svg' where id = 11;
update feature set icon = 'gopro.svg' where id = 12;
update feature set icon = 'poker.svg' where id = 13;

# --- !Downs
delete from feature where id = 12 or id = 13;

# --- !Ups
ALTER TABLE `resort_feature` ADD `available` TINYINT(1)  NULL  DEFAULT '0'  AFTER `feature_resort_id`;
# --- !Downs
ALTER TABLE `resort_feature` DROP `available`;

# --- !Ups
ALTER TABLE `event` DROP FOREIGN KEY `event_company_fk`;
ALTER TABLE `event` CHANGE `company_id` `company_id` INT(11)  UNSIGNED  NOT NULL;
ALTER TABLE `event` ADD CONSTRAINT `event_company_fk` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);


# --- !Downs
